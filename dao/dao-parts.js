const mongoose = require('mongoose');

// Load Model
require('../models/Parts');
require('../models/PartBrands');
require('../models/PartFamilies');
require('../models/PartGroups');

const Collection = mongoose.model('parts');

var newModule = {
    listAll: (fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        let exclude = {
            password: 0
        }
        Collection.find({}, exclude)
        .populate({
            path: 'partGroup'
        })
        .populate({
            path: 'partFamily'
        })
        .populate({
            path: 'partBrand'
        })
        .then(params => {
            fncSuccess(params);
        }).catch((err) => {
            console.log(`caught the error1: ${err.stack}`);
            fncError(err.stack);
        });
    }
};

module.exports = newModule;