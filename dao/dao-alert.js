const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

// Load Model
require('../models/Alert');
require('../models/SystemUsers');
require('../models/CompanyUsers');
require('../models/CustomerUsers');
require('../models/Companies');

const Collection = mongoose.model('alerts');

var newModule = {
    filter: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let search = [];
        let paramsCompany = [];
        

        paramsCompany.push({ $eq: [ "$_id", "$$idcompany" ] });
        if (query.company){
            paramsCompany.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.company) ] })
        } 

        try{//check if the date is valid
            if (query.beginDate){
                let date = new Date(query.beginDate);
                date.toISOString();
                date.setHours(0,0,0,0);
                query.beginDate = date;
            }
            if (query.endDate){
                let date = new Date(query.endDate);
                date.toISOString();
                date.setHours(23,59,59,999);
                query.endDate = date;
            }
        }catch(ex){
        }

        if (query.beginDate){
            params.push({$gte: [ "$date", new Date(query.beginDate) ] });
        }
        if (query.endDate){
            params.push({$lte: [ "$date", new Date(query.endDate) ] });
        }
        if (query.status){
            params.push({$eq: [ "$status",  query.status]});
        }else{
            params.push({$ne: [ "$status",  null]});
        }
        
        search = [
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idsystemUser: '$$CURRENT.systemUser'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: { $eq: [ "$_id", "$$idsystemUser" ] }
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: 'company-users',
                                let: {
                                    idcompanyUser: '$$CURRENT.companyUser'
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr:{
                                                $and: [
                                                    { $eq: [ "$_id", "$$idcompanyUser" ] }
                                                ]
                                            }
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: 'companies',
                                            let: {
                                                idcompany: '$$CURRENT.company'
                                            },
                                            pipeline: [
                                                {
                                                    $match: {
                                                        $expr:{
                                                            $and: paramsCompany
                                                        }
                                                    }
                                                },
                                            ],
                                            as: 'company'
                                        }
                                    },
                                    {
                                        $unwind:{
                                            "path": "$company",
                                            "preserveNullAndEmptyArrays": query.company==null
                                        }
                                    }
                                    // {
                                    //     $match: {
                                    //         $expr:{
                                    //             $and: [
                                    //                 { company : { $ne: null } }
                                    //             ]
                                    //         }
                                    //     }
                                    // }
                                ],
                                as: 'companyUser'
                            }
                        },
                        {
                            $unwind:{
                                "path": "$companyUser",
                                "preserveNullAndEmptyArrays": query.company==null
                            }
                        }
                    ],
                    as: 'systemUser'
                }
            },
            {
                $unwind:"$systemUser"
            },
            {
                $match: { 
                    $expr:{ 
                        $and: params
                    }
                }
            }
        ]

        if (query.limit!=null && query.skip!=null){
            search.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                        totalCount: [{
                            $count: 'count'
                        }
                    ]
                }
            });
        }
    
        Collection.aggregate (search)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;