const mongoose = require("mongoose");
const status = require("../helpers/map-status");

// Load Model
require("../models/Cards");
const Collection = mongoose.model("cards");

var newModule = {
    setAllMainToFalse: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.updateMany({ systemUser: element.systemUser }, { $set: { isMain: false } })
        .then(response => {
            fncSuccess(response);
        }).catch((err) => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            fncError(err.stack);
        });
    },
    setMain: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        
        Collection.updateOne({ _id: element._id }, { $set: { isMain: true } })
        .then(response => {
            fncSuccess(response);
        }).catch((err) => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            fncError(err.stack);
        });
    },
    getMain: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        
        Collection.findOne({ systemUser: element.systemUser, isMain: true })
        .then(response => {
            fncSuccess(response);
        }).catch((err) => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            fncError(err.stack);
        });
    },
    listAllByUser: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        
        Collection.find({ systemUser: element.systemUser})
        .then(response => {
            fncSuccess(response);
        }).catch((err) => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            fncError(err.stack);
        });
    },
    deleteByUser: async (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            let response = await Collection.deleteMany({systemUser: element.systemUser});
            if (fncSuccess)
                return fncSuccess(response);

            return response;    
        } catch (err) {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            if (fncError)
                return fncError(err.stack);

            throw err;
        }
    },
    companySetAllMainToFalse: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.updateMany({ company: element.company }, { $set: { isMain: false } })
        .then(response => {
            fncSuccess(response);
        }).catch((err) => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            fncError(err.stack);
        });
    },
    companyGetMain: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        
        Collection.findOne({ company: element.company, isMain: true })
        .then(response => {
            fncSuccess(response);
        }).catch((err) => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            fncError(err.stack);
        });
    },
    companyListAllByUser: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        
        Collection.find({ company: element.company})
        .then(response => {
            fncSuccess(response);
        }).catch((err) => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            fncError(err.stack);
        });
    },
    deleteByCompany: async (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            let response = await Collection.deleteMany({company: element.company});
            if (fncSuccess)
                return fncSuccess(response);

            return response;    
        } catch (err) {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            if (fncError)
                return fncError(err.stack);

            throw err;
        }
    }
};

module.exports = newModule;