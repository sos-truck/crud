const mongoose = require('mongoose');
// Load Model
require('../models/SystemUsers');
require('../models/CustomerUsers');
require('../models/Companies');
const Collection = mongoose.model('healths');

var newModule = {

    filter: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let pipelineHealths = [];

        if(query.status){
            params.push({ status : query.status });
        }else{  
            params.push({ status : 'ACTIVE' });
        }

        if (query.trucker) {
            params.push({ 'trucker._id' : mongoose.Types.ObjectId(query.trucker) });
        }

        pipelineHealths = [
           
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idsystem: '$$CURRENT.trucker'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: [
                                        { $eq: [ "$_id", "$$idsystem" ] },
                                    ]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: 'customer-users',
                                let: {
                                    idcustomer: '$$CURRENT.customerUser'
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr:{
                                                $and: [
                                                    { $eq: [ "$_id", "$$idcustomer" ] },
                                                ]
                                            }
                                        }
                                    }
                                ],
                                as: 'customerUser'
                            }
                        },
                        {
                            $unwind:{
                                "path": "$customerUser",
                                "preserveNullAndEmptyArrays": true
                            }
                        },
                    ],
                    as: 'trucker'
                }
            },
            {
                $unwind:{
                    "path": "$trucker",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $match: { 
                    $and: params
                }
            }
        
        ];

    
        Collection.aggregate (pipelineHealths)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;