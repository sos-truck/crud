const mongoose = require('mongoose');

// Load Model
require('../models/IndicationCodes');
require('../models/SystemUsers');
const Collection = mongoose.model('indication-codes');

var newModule = {
    findOwnerByCode: (code, fncSuccess, fncError) => {
        Collection.findOne({code: code})
        .populate({
            path: 'owner'
        })
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncError(err.stack);
        });
    },
    getMyCupom: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let pipelineCodes = [];
        let paramsOwner = [];

        if (query.owner) {
            paramsOwner.push({ $eq: [ "$owner", mongoose.Types.ObjectId(query.owner) ] })
        }
        params.push({ status: 'ACTIVE' });
        pipelineCodes = [
                {
                    $match: {
                        $expr:{
                            $and: paramsOwner
                        }
                    }
                },
            ];

        Collection.aggregate (pipelineCodes)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    },
    filter: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let pipelineCodes = [];

        params.push({ status: 'ACTIVE' });

        pipelineCodes = [
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idowner: '$$CURRENT.owner'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: [
                                        { $eq: [ "$_id", "$$idowner" ] },
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'owner'
                }
            },
            {
                $unwind:"$owner"
            },
            {
                $match: { 
                    $and: params
                }
            }
            
        ];
 

        if (query.limit!=null && query.skip!=null){
            console.log('query.limit && query.skip');

            pipelineCompany.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                    totalCount: [{
                            $count: 'count'
                    }]
                }
            });
        }
    
        Collection.aggregate (pipelineCodes)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;