const mongoose = require('mongoose');

// Load Model
require('../models/Trucks');
require('../models/Certified');
require('../models/CertifiedItem');
require('../models/SystemUsers');
require('../models/CustomerUsers');
require('../models/Manufacturers');

const Collection = mongoose.model('trucks');

var newModule = {

    listWithoutCertifie: (fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.find({
            certifies: []  })
        .populate({
            path: 'manufacturer'
        })
        .populate({
            path: 'model'
        })
        .populate({
            path: 'truckDriver',
            populate: [
                { 
                    path: 'customerUser'
                }
            ]
        })
        .then(params => {
            fncSuccess(params);
        }).catch((err) => {
            console.log(`caught the error1: ${err.stack}`);
            fncError(err.stack);
        });
    },
    listAllWithCertifieExpired: (fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.find({
            certifies: {
                $ne: []
            }
        })
        .populate({
            path: 'certifies',
            populate: [
                { 
                    path: 'certified'
                }
            ]
        })
        .populate({
            path: 'manufacturer'
        })
        .populate({
            path: 'model'
        })
        .populate({
            path: 'truckDriver',
            populate: [
                { 
                    path: 'customerUser'
                }
            ]
        })
        .then(params => {
            fncSuccess(params);
        }).catch((err) => {
            console.log(`caught the error1: ${err.stack}`);
            fncError(err.stack);
        });
    },
    filterCertifieExpired: async (element, fncSuccess) => {
        console.log("element ---->" + JSON.stringify(element.certifie))
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.find({ certified: element.certifie })
        // Collection.find({
        //     certifies: {
        //         $ne: []
        //     },
        // })
        .populate({
            path: 'certifies',
            populate: [
                { 
                    path: 'certified'
                }
            ]
        })
        .populate({
            path: 'manufacturer'
        })
        .populate({
            path: 'model'
        })
        .populate({
            path: 'truckDriver',
            populate: [
                { 
                    path: 'customerUser'
                }
            ]
        })
        .then(params => {
            fncSuccess(params);
        }).catch((err) => {
            console.log(`caught the error1: ${err.stack}`);
            fncError(err.stack);
        });
    },
    filter: async (query) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            
   
            let pipeline = [];
            let params = [];
            let paramsName = [];
            let paramsType = [];

            if(query.status){
                params.push({ 'status': query.status });
            }else{
                params.push({ 'status': 'ACTIVE' });
            }

            if (query.company) {
                params.push({ 'company' : mongoose.Types.ObjectId(query.company) });
            }

            if (query.systemUser) {
                params.push({ 'systemUser' : mongoose.Types.ObjectId(query.systemUser) });
            }
            
            if (query.truckDriver) {
                params.push({ 'truckDriver' : mongoose.Types.ObjectId(query.truckDriver) });
            }

            if(query.vehicleTypes){
                paramsType.push({ $in: ['$vehicleType', query.vehicleTypes] });
            }

            if (query.manufacturer) {
                params.push({ 'manufacturer' : mongoose.Types.ObjectId(query.manufacturer) });
            }

            if (query.model) {
                params.push({ 'model' : mongoose.Types.ObjectId(query.model) });
            }

            if(query.plate){
                paramsName.push({
                    $or:[{
                            $or:[{
                                plate: {
                                    $regex: query.plate.toLowerCase(), $options: "i"
                                }
                            }]
                        }
                    ]
                });
            }

            pipeline = [
                {
                    $match: {
                        $expr:{
                            $and: paramsType
                        }
                    }
                },
                {
                    $match: { 
                        $and: params
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idSystemUser: '$$CURRENT.systemUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: [
                                            { $eq: [ "$_id", "$$idSystemUser" ] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'customer-users',
                                    let: {
                                        idcustomer: '$$CURRENT.customerUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: [
                                                        { $eq: [ "$_id", "$$idcustomer" ] }
                                                    ]
                                                }
                                            }
                                        },
                                    ],
                                    as: 'customerUser'
                                }
                            },
                            {
                                $unwind:{
                                    "path": "$customerUser",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                        ],
                        as: 'systemUser'
                    }
                },
                {
                    $unwind:{
                        "path": "$systemUser",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idTruckDriver: '$$CURRENT.truckDriver'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: [
                                            { $eq: [ "$_id", "$$idTruckDriver" ] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'customer-users',
                                    let: {
                                        idcustomer2: '$$CURRENT.customerUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: [
                                                        { $eq: [ "$_id", "$$idcustomer2" ] }
                                                    ]
                                                }
                                            }
                                        },
                                    ],
                                    as: 'customerUser'
                                }
                            },
                            {
                                $unwind:{
                                    "path": "$customerUser",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                        ],
                        as: 'truckDriver'
                    }
                },
                {
                    $unwind:{
                        "path": "$truckDriver",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'models',
                        let: {
                            idmodel: '$$CURRENT.model'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: { $eq: [ "$_id", "$$idmodel" ] }
                                    }
                                }
                            }
                        ],
                        as: 'model'
                    }
                },
                {
                    $unwind:{
                        "path": "$model",
                        "preserveNullAndEmptyArrays": true
                    }
                },     
                {
                    $lookup: {
                        from: 'manufacturers',
                        let: {
                            idmanufacturer: '$$CURRENT.manufacturer'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: { $eq: [ "$_id", "$$idmanufacturer" ] }
                                    }
                                }
                            }
                        ],
                        as: 'manufacturer'
                    }
                },
                {
                    $unwind:{
                        "path": "$manufacturer",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'trucks',
                        let: {
                            idtruckTrailers: '$$CURRENT.truckTrailers'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: { $eq: [ "$_id", "$$idtruckTrailers" ] }
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idmanufacturer2: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idmanufacturer2" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind:{
                                    "path": "$manufacturer",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                            {
                                $lookup: {
                                    from: 'certified-items',
                                    let: {
                                        idcertifies2: '$$CURRENT.certifies'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: [
                                                        { $in: ['$_id', "$$idcertifies2"] }
                                                    ]
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: 'certifies',
                                                let: {
                                                    idcertified2: '$$CURRENT.certified'
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr:{
                                                                $and: { $eq: [ "$_id", "$$idcertified2" ] }
                                                            }
                                                        }
                                                    }
                                                ],
                                                as: 'certified'
                                            }
                                        },
                                        {
                                            $unwind:{
                                                "path": "$certified",
                                                "preserveNullAndEmptyArrays": true
                                            }
                                        },
                                    ],
                                    as: 'certifies'
                                }
                            }
                        ],
                        as: 'truckTrailers'
                    }
                },
                {
                    $lookup: {
                        from: 'certified-items',
                        let: {
                            idcertifies: '$$CURRENT.certifies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: [
                                            { $in: ['$_id', "$$idcertifies"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'certifies',
                                    let: {
                                        idcertified: '$$CURRENT.certified'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcertified" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'certified'
                                }
                            },
                            {
                                $unwind:{
                                    "path": "$certified",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                        ],
                        as: 'certifies'
                    }
                },
                {
                    $lookup: {
                        from: 'companies',
                        let: {
                            idCompany: '$$CURRENT.company'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idCompany"] }
                                        ]
                                    }
                                }
                            },
                            { $project: { fantasyName: "$fantasyName", document: "$document", primaryPhone: "$primaryPhone"} },
                        ],
                        as: 'company'
                    },
                },
                {
                    $unwind: {
                        "path": "$company",
                        "preserveNullAndEmptyArrays": true
                    }
                }
            ];

            let obj = await Collection.aggregate(pipeline);

            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listAllByFilters: async (query) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        
        try {
            let params = [];
            let paramsTrucks = [];
            let paramsCompany = [];
            let paramsSystemUsers = [];
            let paramsTruckDriver = [];
            let paramsManufacturer = [];
            let paramsModel = [];
            let search = [];

            paramsSystemUsers.push({ $eq: [ "$_id", "$$idsystemUser" ] });
            paramsTruckDriver.push({ $eq: [ "$_id", "$$idtruckDriver" ] });
            paramsTrucks.push({ $in: ['$_id', "$$idtruckTrailers"] });
            paramsManufacturer.push({ $eq: [ "$_id", "$$idmanufacturer" ] });
            paramsModel.push({ $eq: [ "$_id", "$$idmodel" ] });
            
            if (query.company){
                paramsCompany.push({ $eq: [ "$company", mongoose.Types.ObjectId(query.company) ] })
            } 
            
            if (query.status){
                params.push({$eq: [ "$status",  query.status]});
            }else{
                params.push({$ne: [ "$status",  null]});
            }

            if (query.systemUser) {
                paramsSystemUsers.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.systemUser) ] });
            }

            if (query.truckDriver){
                paramsTruckDriver.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.truckDriver) ] });
            }

            if (query.vehicleTypes){
                params.push({ $in: ['$vehicleType', query.vehicleTypes] });
            }

            if (query.manufacturer){
                paramsManufacturer.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.manufacturer) ] });
            }

            if (query.model){
                paramsModel.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.model) ] });
            }

            let paramsName = [];
            if(query.plate){
                paramsName.push({
                    $or:[{
                            $or:[{
                                plate: {
                                    $regex: query.plate.toLowerCase(), $options: "i"
                                }
                            }]
                        }
                    ]
                });
               
            }
            
            search = [
                {
                    $lookup: {
                        from: 'models',
                        let: {
                            idmodel: '$$CURRENT.model'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: { $eq: [ "$_id", "$$idmodel" ] }
                                    }
                                }
                            }
                        ],
                        as: 'model'
                    }
                },
                {
                    $unwind:{
                        "path": "$model",
                        "preserveNullAndEmptyArrays": true
                    }
                },     
                {
                    $lookup: {
                        from: 'manufacturers',
                        let: {
                            idmanufacturer: '$$CURRENT.manufacturer'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: { $eq: [ "$_id", mongoose.Types.ObjectId(query.manufacturer) ] }
                                    }
                                }
                            }
                        ],
                        as: 'manufacturer'
                    }
                },
                {
                    $unwind:{
                        "path": "$manufacturer",
                        "preserveNullAndEmptyArrays": false
                    }
                },
                {
                    $lookup: {
                        from: 'trucks',
                        let: {
                            idtruckTrailers: '$$CURRENT.truckTrailers'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsTrucks
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idmanufacturer2: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idmanufacturer2" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind:"$manufacturer"
                            },
                            {
                                $lookup: {
                                    from: 'certified-items',
                                    let: {
                                        idcertifies2: '$$CURRENT.certifies'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: [
                                                        { $in: ['$_id', "$$idcertifies2"] }
                                                    ]
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: 'certifies',
                                                let: {
                                                    idcertified2: '$$CURRENT.certified'
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr:{
                                                                $and: { $eq: [ "$_id", "$$idcertified2" ] }
                                                            }
                                                        }
                                                    }
                                                ],
                                                as: 'certified'
                                            }
                                        },
                                        {
                                            $unwind:"$certified"
                                        }
                                    ],
                                    as: 'certifies'
                                }
                            }
                        ],
                        as: 'truckTrailers'
                    }
                },
                {
                    $lookup: {
                        from: 'certified-items',
                        let: {
                            idcertifies: '$$CURRENT.certifies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: [
                                            { $in: ['$_id', "$$idcertifies"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'certifies',
                                    let: {
                                        idcertified: '$$CURRENT.certified'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcertified" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'certified'
                                }
                            },
                            {
                                $unwind:"$certified"
                            }
                        ],
                        as: 'certifies'
                    }
                },
                {
                    $lookup: {
                        from: 'companies',
                        let: {
                            idCompany: '$$CURRENT.company'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idCompany"] }
                                        ]
                                    }
                                }
                            },
                            { $project: { fantasyName: "$fantasyName", document: "$document", primaryPhone: "$primaryPhone"} },
                        ],
                        as: 'company'
                    },
                },
                {
                    $unwind: {
                        "path": "$company",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idsystemUser: '$$CURRENT.systemUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsSystemUsers
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'customer-users',
                                    let: {
                                        idcustomerUser: '$$CURRENT.customerUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcustomerUser" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'customerUser'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$customerUser",
                                    "preserveNullAndEmptyArrays": true
                                }
                            }
                        ],
                        as: 'systemUser'
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idtruckDriver: '$$CURRENT.truckDriver'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsTruckDriver
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'customer-users',
                                    let: {
                                        idcustomerUser2: '$$CURRENT.customerUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcustomerUser2" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'customerUser'
                                }
                            },
                            {
                                $unwind:"$customerUser"
                            }
                        ],
                        as: 'truckDriver'
                    }
                },
                {
                    $match: { 
                        $expr:{ 
                            $and: params
                        }
                    }
                },
                {
                    $match: {
                        $expr:{
                            $and: paramsCompany
                        }
                    }
                }
        
            ]

            if (query.limit!=null && query.skip!=null){
                search.push({
                    $facet: {
                        paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                            totalCount: [{
                                $count: 'count'
                            }
                        ]
                    }
                });
            }

            if (query.systemUser || query.truckDriver){
                
                if (query.systemUser){
                    search.push({
                        $unwind: '$systemUser'
                    });

                    search.push({$unwind: {
                        "path": "$truckDriver",
                        "preserveNullAndEmptyArrays": true
                    }});
                }

                if (query.truckDriver){
                    search.push({
                        $unwind: '$truckDriver'
                    });
                    search.push({$unwind: {
                        "path": "$systemUser",
                        "preserveNullAndEmptyArrays": true
                    }});
                }
            }else{
                search.push({
                    $unwind:{
                        "path": "$truckDriver",
                        "preserveNullAndEmptyArrays": true
                    }
                });
                
                search.push({
                    $unwind:{
                        "path": "$systemUser",
                        "preserveNullAndEmptyArrays": true
                    }
                });
            }

            if(query.plate){
                search.push(
                    {
                        $match: {
                            $and: paramsName
                        }
                    }
                )
            }

            let obj = await Collection.aggregate (search);

            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);

            throw error;
        }
    },
    listGroupByManufacturer: async (query) => {
        try {
            if (!isDbConnected) throw ({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
            let params = [];
            let search = [];

            
            if (query.status){
                params.push({ status: query.status });
            }else{
                params.push({ status: 'ACTIVE' });
            }
        
            if (query.vehicleType){
                params.push({ $in: ['$vehicleType', query.vehicleType] });
            }
         
            search = [
                {
                    $match: { 
                        $expr:{ 
                            $and: params
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'manufacturers',
                        let: {
                            idmanufacturer: '$$CURRENT.manufacturer'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: { $eq: [ "$_id", "$$idmanufacturer" ] }
                                    }
                                }
                            }
                        ],
                        as: 'manufacturer'
                    }
                },
                {
                    $unwind: {
                        "path": "$manufacturer",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $group: {
                        _id: { manufacturer :  "$manufacturer" },
                        nTrucks: {$sum: 1}
                    }
                },
                {
                    $sort : { nTrucks: -1 }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listCertifieExpired: async (query) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            let params = [];
            let paramsTrucks = [];
            let paramsCompany = [];
            let paramsSystemUsers = [];
            let paramsTruckDriver = [];
            let paramsManufacturer = [];
            let paramQtd= [];
            
            let search = [];

            paramsSystemUsers.push({ $eq: [ "$_id", "$$idsystemUser" ] });
            paramsTruckDriver.push({ $eq: [ "$_id", "$$idtruckDriver" ] });
            paramsTrucks.push({ $in: ['$_id', "$$idtruckTrailers"] });
            paramsManufacturer.push({ $eq: [ "$_id", "$$idmanufacturer" ] });

            try{//check if the date is valid
                if (query.date){
                    let date = new Date(query.date);
                    date.toISOString();
                    date.setHours(0,0,0,0);
                    query.date = date;
                }
            }catch(ex){
            }
    
            paramQtd.push({ $ne: ["$certifies", [] ]} );

            if (query.company){
                paramsCompany.push({ $eq: [ "$company", mongoose.Types.ObjectId(query.company) ] })
            } 
            
            if (query.status){
                params.push({$eq: [ "$status",  query.status]});
            }else{
                params.push({$ne: [ "$status",  null]});
            }
          
            if (query.company) {
                params.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.company) ] });
            }

            if (query.systemUser) {
                paramsSystemUsers.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.systemUser) ] });
            }

            if (query.truckDriver){
                paramsTruckDriver.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.truckDriver) ] });
            }

            if (query.vehicleTypes){
                params.push({ $in: ['$vehicleType', query.vehicleTypes] });
            }

            if (query.manufacturer){
                paramsManufacturer.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.manufacturer) ] });
            }

            search = [
                {
                    $match: { 
                        $expr:{ 
                            $and: { $ne: ["$certifies", "" ]}
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'manufacturers',
                        let: {
                            idmanufacturer: '$$CURRENT.manufacturer'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsManufacturer
                                    }
                                }
                            }
                        ],
                        as: 'manufacturer'
                    }
                },
                {
                    $unwind:{
                        "path": "$manufacturer",
                        "preserveNullAndEmptyArrays": false
                    }
                },
                {
                    $lookup: {
                        from: 'models',
                        let: {
                            idmodel: '$$CURRENT.model'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsModel
                                    }
                                }
                            }
                        ],
                        as: 'model'
                    }
                },
                {
                    $unwind:{
                        "path": "$model",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'trucks',
                        let: {
                            idtruckTrailers: '$$CURRENT.truckTrailers'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsTrucks
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idmanufacturer2: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idmanufacturer2" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind:"$manufacturer"
                            },
                            {
                                $lookup: {
                                    from: 'certified-items',
                                    let: {
                                        idcertifies2: '$$CURRENT.certifies'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: [
                                                        { $in: ['$_id', "$$idcertifies2"] }
                                                    ]
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: 'certifies',
                                                let: {
                                                    idcertified2: '$$CURRENT.certified'
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr:{
                                                                $and: { $eq: [ "$_id", "$$idcertified2" ] }
                                                            }
                                                        }
                                                    }
                                                ],
                                                as: 'certified'
                                            }
                                        },
                                        {
                                            $unwind:"$certified"
                                        }
                                    ],
                                    as: 'certifies'
                                }
                            }
                        ],
                        as: 'truckTrailers'
                    }
                },
                {
                    $lookup: {
                        from: 'certified-items',
                        let: {
                            idcertifies: '$$CURRENT.certifies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: [
                                            { $in: ['$_id', "$$idcertifies"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'certifies',
                                    let: {
                                        idcertified: '$$CURRENT.certified'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcertified" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'certified'
                                }
                            },
                            {
                                $unwind:"$certified"
                            }
                        ],
                        as: 'certifies'
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idsystemUser: '$$CURRENT.systemUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsSystemUsers
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'company-users',
                                    let: {
                                        idcompanyUser: '$$CURRENT.companyUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcompanyUser" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'companyUser'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$companyUser",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                            {
                                $lookup: {
                                    from: 'customer-users',
                                    let: {
                                        idcustomerUser: '$$CURRENT.customerUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcustomerUser" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'customerUser'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$customerUser",
                                    "preserveNullAndEmptyArrays": true
                                }
                            }
                        ],
                        as: 'systemUser'
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idtruckDriver: '$$CURRENT.truckDriver'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr:{
                                        $and: paramsTruckDriver
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'customer-users',
                                    let: {
                                        idcustomerUser2: '$$CURRENT.customerUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr:{
                                                    $and: { $eq: [ "$_id", "$$idcustomerUser2" ] }
                                                }
                                            }
                                        }
                                    ],
                                    as: 'customerUser'
                                }
                            },
                            {
                                $unwind:"$customerUser"
                            }
                        ],
                        as: 'truckDriver'
                    }
                },
                {
                    $match: { 
                        $expr:{ 
                            $and: params
                        }
                    }
                },
                {
                    $match: {
                        $expr:{
                            $and: paramsCompany
                        }
                    }
                }
        
            ]

            if (query.limit!=null && query.skip!=null){
                search.push({
                    $facet: {
                        paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                            totalCount: [{
                                $count: 'count'
                            }
                        ]
                    }
                });
            }

            if (query.systemUser || query.truckDriver){
                
                if (query.systemUser){
                    search.push({
                        $unwind: '$systemUser'
                    });

                    search.push({$unwind: {
                        "path": "$truckDriver",
                        "preserveNullAndEmptyArrays": true
                    }});
                }

                if (query.truckDriver){
                    search.push({
                        $unwind: '$truckDriver'
                    });
                    search.push({$unwind: {
                        "path": "$systemUser",
                        "preserveNullAndEmptyArrays": true
                    }});
                }
            }else{
                search.push({
                    $unwind:{
                        "path": "$truckDriver",
                        "preserveNullAndEmptyArrays": true
                    }
                });
                
                search.push({
                    $unwind:{
                        "path": "$systemUser",
                        "preserveNullAndEmptyArrays": true
                    }
                });
            }

            let obj = await Collection.aggregate (search);

            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);

            throw error;
        }
    }
    
};


module.exports = newModule;