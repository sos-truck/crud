const mongoose = require('mongoose');
// Load Model
require('../models/IndicationUses');
require('../models/SystemUsers');
require('../models/CompanyUsers');
require('../models/Companies');
const Collection = mongoose.model('indication-uses');

var newModule = {

    filter: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let pipelineCodeUses = [];

        if(query.status){
            params.push({ status : query.status });
        }else{
            params.push({ status : 'ACTIVE' });
        }

        if (query.systemUser) {
            params.push({ 'systemUser._id' : mongoose.Types.ObjectId(query.systemUser) });
        }
        params.push({ 'systemUser.status' : 'ACTIVE'});

        if (query.owner) {
            params.push({ 'indicationcoderef.owner._id' : mongoose.Types.ObjectId(query.owner) });
        }

        pipelineCodeUses = [
           
            {
                $lookup: {
                    from: 'indication-codes',
                    let: {
                        idcode: '$$CURRENT.indicationcoderef'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: [
                                        { $eq: [ "$_id", "$$idcode" ] },
                                    ]
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: 'system-users',
                                let: {
                                    idowner: '$$CURRENT.owner'
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr:{
                                                $and: [
                                                    { $eq: [ "$_id", "$$idowner" ] },
                                                ]
                                            }
                                        }
                                    }
                                ],
                                as: 'owner'
                            }
                        },
                        {
                            $unwind:{
                                "path": "$owner",
                                "preserveNullAndEmptyArrays": true
                            }
                        },
                    ],
                    as: 'indicationcoderef'
                }
            },
            {
                $unwind:{
                    "path": "$indicationcoderef",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        iduser: '$$CURRENT.systemUser'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: [
                                        { $eq: [ "$_id", "$$iduser" ] },
                                    ]
                                }
                            }
                        },
                        
                    ],
                    as: 'systemUser'
                }
            },
            {
                $unwind:{
                    "path": "$systemUser",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $match: { 
                    $and: params
                }
            }
        
        ];

    
        Collection.aggregate (pipelineCodeUses)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;