const mongoose = require('mongoose');

// Load Model
require('../models/Banner');
const Collection = mongoose.model('banners');

var newModule = {
    list: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.find({ status: query.status })
        .sort({priority: 1})
        .then(params => {
            fncSuccess(params);
        }).catch((err) => {
            console.log(`caught the error1: ${err.stack}`);
            fncError(err.stack);
        });
    }
};

module.exports = newModule;