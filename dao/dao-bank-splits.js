const mongoose = require('mongoose');
// Load Model
require('../models/BankSlips');
require('../models/Orders');
require('../models/Companies');
const Collection = mongoose.model('bank-slips');

var newModule = {

    filter: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let paramsDate = [];
        let pipeline = [];

        if(query.beginDate && query.endDate){
            let date = new Date(query.beginDate);
            date.toLocaleString();
            date.setHours(0, 0, 0, 0);
            let beginDate = date;
    
            let dateEnd = new Date(query.endDate);
            dateEnd.toLocaleString();
            dateEnd.setHours(23, 59, 59, 999);
            let endDate = dateEnd;
    
            paramsDate.push({ $gte: ["$dateOrder", new Date(beginDate)] });
            paramsDate.push({ $lte: ["$dateOrder", new Date(endDate)] });
        }

        params.push({ 'order.statusOrder': { $exists: true } });

        if(query.status){
            params.push({ 'order.statusOrder' : query.status });
        }

        if (query.order) {
            params.push({ 'order._id' : mongoose.Types.ObjectId(query.order) });
        }

        if (query.company) {
            params.push({ 'company._id' : mongoose.Types.ObjectId(query.company) });
        }


        pipeline = [
            {
                $match: {
                    $expr: {
                        $and: paramsDate
                    }
                }
            },
            {
                $lookup: {
                    from: 'companies',
                    let: {
                        idCompany: '$$CURRENT.company'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$_id", "$$idCompany"] }
                                    ]
                                }
                            }
                        },
                    ],
                    as: 'company'
                }
            },
            {
                $unwind: {
                    "path": "$company",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: 'orders',
                    let: {
                        idOrders: '$$CURRENT.order'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$_id", "$$idOrders"] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'order'
                }
            },
            {
                $unwind: {
                    "path": "$order",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $match: { 
                    $and: params
                }
            },
            { $sort: { createDate: -1 } }
        
        ];

    
        Collection.aggregate (pipeline)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;