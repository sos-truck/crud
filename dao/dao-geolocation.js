const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

// Load Model
require('../models/Geolocation');
require('../models/SystemUsers');
require('../models/CompanyUsers');
require('../models/CustomerUsers');
require('../models/Companies');

const Collection = mongoose.model('geolocations');

var newModule = {
    filter: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
       
    
        
        Collection.findOne({systemUser:query.systemUserId},{__v:0})
        .populate({
            path: 'systemUser'
        })
        .sort({date:-1})
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;