const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

// Load Model
require('../models/OrdersHolder');
require('../models/Orders');
require('../models/Trucks');
require('../models/SystemUsers');
require('../models/CompanyUsers');
require('../models/PartFamilies');
require('../models/Manufacturers');
require('../models/Companies');
require('../models/Addresses');
require('../models/Models');

const Collection = mongoose.model('orders-holder');

var newModule = {

    changeStatusElements: async (element) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        try {
            let response = await Collection.updateOne({ _id: element.orderHolderId }, { $set: { statusOrder: element.statusOrder, justifyClose: element.justifyClose } });

            return response;
        } catch (err) {
            console.log(`caught the error: ${err.stack}`);
            throw err;
        }
    },
    getHolderByOrder: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            let params = [];
            let paramsOrder = [];
            let search = [];

            paramsOrder.push({ $in: ['$_id', "$$idorders"] });

            params.push({ status: 'ACTIVE' });

            if (query.order) {
                paramsOrder.push({ $in: [mongoose.Types.ObjectId(query.order), "$$idorders"] });
                params.push({ orders: { $ne: null } });
                params.push({ orders: { $ne: [] } });

            } else if (query.isHideEmptyOrders) {
                params.push({ orders: { $ne: null } });
                params.push({ orders: { $ne: [] } });
            }

            search = [
                {
                    $match: {
                        $and: [
                            { orders: { $type: 'array' } }
                        ]
                    }
                },
                {
                    $match: {
                        $and: params
                    }
                }
            ]

            let obj;
            obj = await Collection.aggregate(search)

            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listByUserWithoutLookup: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

            console.log('##### busca #####')

            let params = [];
            let search = [];

            params.push({ status: 'ACTIVE' });

            if (query._id) {
                params.push({ _id: mongoose.Types.ObjectId(query._id) });
            }

            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }
        
            if (query.typeOrder) {
                params.push({ typeOrder: query.typeOrder });
            }

            if (query.systemUser) {
                params.push({ systemUser: mongoose.Types.ObjectId(query.systemUser) });
            }

            search = [
                {
                    $match: {
                        $and: [
                            { orders: { $type: 'array' } }
                        ]
                    }
                },
                {
                    $match: {
                        $and: params
                    }
                },
            ]

            search.push(
                {
                    $lookup: {
                        from: 'manufacturers',
                        let: {
                            idmanufacturer: '$$CURRENT.manufacturer'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idmanufacturer"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'manufacturer'
                    }
                },
                {
                    $unwind: {
                        "path": "$manufacturer",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'models',
                        let: {
                            idtruckModel: '$$CURRENT.truckModel'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idtruckModel"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'truckModel'
                    }
                },
                {
                    $unwind: {
                        "path": "$truckModel",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'trucks',
                        let: {
                            idtruck: '$$CURRENT.truck'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idtruck"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idmanufacturer: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idmanufacturer"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$manufacturer",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                            {
                                $lookup: {
                                    from: 'models',
                                    let: {
                                        idmodel: '$$CURRENT.model'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idmodel"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'model'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$model",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                        ],
                        as: 'truck'
                    }
                },
                {
                    $unwind: {
                        "path": "$truck",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'certifies',
                        let: {
                            idcertified: '$$CURRENT.certified'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idcertified"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'certified'
                    }
                },
                {
                    $unwind: {
                        "path": "$certified",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        let: {
                            idserviceFamilies: '$$CURRENT.serviceFamilies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idserviceFamilies"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'serviceFamilies'
                    }
                },
                {
                    $unwind: {
                        "path": "$serviceFamilies",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idsystemUser: '$$CURRENT.systemUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: { $eq: ["$_id", "$$idsystemUser"] }
                                    }
                                }
                            },
                            { $project: { firstName: "$firstName", lastName: "$lastName", email: "$email", primaryPhone: "$primaryPhone", type: "$type"} },
                        ],
                        as: 'systemUser'
                    },
               
                },
                {
                    $unwind: {
                        "path": "$systemUser",
                    }
                },
                {
                    $lookup: {
                        from: 'orders',
                        let: {
                            idorders: '$$CURRENT.orders'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: { $in: ['$_id', "$$idorders"] }
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'companies',
                                    let: {
                                        idcompany: '$$CURRENT.company'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: { $eq: ["$_id", "$$idcompany"] }
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: 'addresses',
                                                let: {
                                                    idaddress: '$$CURRENT.address'
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr: {
                                                                $and: [
                                                                    { $eq: ["$_id", "$$idaddress"] }
                                                                ]
                                                            }
                                                        }
                                                    }
                                                ],
                                                as: 'address'
                                            }
                                        },
                                        {
                                            $unwind: {
                                                "path": "$address",
                                                "preserveNullAndEmptyArrays": true
                                            }
                                        },
                                    { $project: { fantasyName: "$fantasyName", socialName: "$socialName", email: "$email", primaryPhone: "$primaryPhone", document: "$document", address: "$address", status: "$status"}},
                                    ],
                                    as: 'company'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$company",
                                    "preserveNullAndEmptyArrays": true
                                }
                            }
                        ],
                        as: 'orders'
                    }
                }
            )

            search.push({ $sort: { dateOrder: -1 } });

            let obj;

            if (query.limit != null && query.skip != null && !query.isCount) {
                let skip = query.skip > 0 ? ((query.skip - 1) * query.limit) : 0;
                obj = await Collection.aggregate(search).skip(skip).limit(query.limit)
            } else {
                obj = await Collection.aggregate(search)
            }

            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listByFilters: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            console.log('#####busca')
            let params = [];
            let paramsDate = [];
            let paramsCompany = [];
            let paramsUser = [];
            let paramsOrder = [];
            let search = [];

            paramsCompany.push({ $eq: ["$_id", "$$idcompany"] });

            paramsUser.push({ $eq: ["$_id", "$$idsystemUser"] });

            paramsOrder.push({ $in: ['$_id', "$$idorders"] });

            params.push({ status: 'ACTIVE' });
            paramsDate.push({ status: 'ACTIVE' });


            try {//check if the date is valid
                if (query.date) {
                    let date = new Date(query.date);
                    date.toISOString();
                    date.setHours(0, 0, 0, 0);
                    query.date = date;
                }
            } catch (ex) {
            }

            if (query.beginDate) {
                paramsDate.push({ $gte: ["$dateOrder", new Date(query.beginDate)] });
            }

            if (query.endDate) {
                paramsDate.push({ $lte: ["$dateOrder", new Date(query.endDate)] });
            }


            if (query._id) {
                params.push({ _id: mongoose.Types.ObjectId(query._id) });
            }

            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }

            // if (query.company) {
            //     paramsCompany.push({ $eq: ["$_id", mongoose.Types.ObjectId(query.company)] });
            // }
            if (query.company) {
                params.push({ company: mongoose.Types.ObjectId(query.company) });
            }

            if (query.typeOrder) {
                params.push({ typeOrder: query.typeOrder });
            }

            if (query.systemUser) {
                paramsUser.push({ $eq: ["$_id", mongoose.Types.ObjectId(query.systemUser)] });
            }

            if (query.order) {
                paramsOrder.push({ $in: [mongoose.Types.ObjectId(query.order), "$$idorders"] });
                params.push({ orders: { $ne: null } });
                params.push({ orders: { $ne: [] } });

            } else if (query.isHideEmptyOrders) {
                params.push({ orders: { $ne: null } });
                params.push({ orders: { $ne: [] } });
            }

            search = [
                { $sort: { dateOrder: -1 } },
                {
                    $match: {
                        $expr: {
                            $and: paramsDate
                        }
                    }
                },
                {
                    $match: {
                        $and: [
                            { orders: { $type: 'array' } }
                        ]
                    }
                },
                {
                    $match: {
                        $and: params
                    }
                },

            ]

            if (query.isCount) {
                search.push({
                    $group: {
                        _id: null,
                        count: {
                            $sum: 1
                        }
                    }
                });
            } else {
                search.push(
                    {
                        $lookup: {
                            from: 'manufacturers',
                            let: {
                                idmanufacturer: '$$CURRENT.manufacturer'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$idmanufacturer"] }
                                            ]
                                        }
                                    }
                                }
                            ],
                            as: 'manufacturer'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$manufacturer",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'models',
                            let: {
                                idtruckModel: '$$CURRENT.truckModel'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$idtruckModel"] }
                                            ]
                                        }
                                    }
                                }
                            ],
                            as: 'truckModel'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$truckModel",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'trucks',
                            let: {
                                idtruck: '$$CURRENT.truck'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$idtruck"] }
                                            ]
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: 'manufacturers',
                                        let: {
                                            idmanufacturer: '$$CURRENT.manufacturer'
                                        },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $and: [
                                                            { $eq: ["$_id", "$$idmanufacturer"] }
                                                        ]
                                                    }
                                                }
                                            }
                                        ],
                                        as: 'manufacturer'
                                    }
                                },
                                {
                                    $unwind: {
                                        "path": "$manufacturer",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },
                                {
                                    $lookup: {
                                        from: 'models',
                                        let: {
                                            idmodel: '$$CURRENT.model'
                                        },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $and: [
                                                            { $eq: ["$_id", "$$idmodel"] }
                                                        ]
                                                    }
                                                }
                                            }
                                        ],
                                        as: 'model'
                                    }
                                },
                                {
                                    $unwind: {
                                        "path": "$model",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },
                            ],
                            as: 'truck'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$truck",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'part-families',
                            let: {
                                idfamily: '$$CURRENT.family'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$idfamily"] }
                                            ]
                                        }
                                    }
                                }
                            ],
                            as: 'family'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$family",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'certifies',
                            let: {
                                idcertified: '$$CURRENT.certified'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$idcertified"] }
                                            ]
                                        }
                                    }
                                }
                            ],
                            as: 'certified'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$certified",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'part-families',
                            let: {
                                idserviceFamilies: '$$CURRENT.serviceFamilies'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$idserviceFamilies"] }
                                            ]
                                        }
                                    }
                                }
                            ],
                            as: 'serviceFamilies'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$serviceFamilies",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'system-users',
                            let: {
                                iduserClose: '$$CURRENT.userClose'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$iduserClose"] }
                                            ]
                                        }
                                    }
                                },
                            ],
                            as: 'userClose'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$userClose",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'system-users',
                            let: {
                                idsystemUser: '$$CURRENT.systemUser'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: paramsUser
                                        }
                                    }
                                }
                            ],
                            as: 'systemUser'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$systemUser",
                        }
                    },
                    {
                        $lookup: {
                            from: 'system-users',
                            let: {
                                idtrucker: '$$CURRENT.trucker'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: [
                                                { $eq: ["$_id", "$$idtrucker"] }
                                            ]
                                        }
                                    }
                                }
                            ],
                            as: 'trucker'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$trucker",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'companies',
                            let: {
                                idcompany: '$$CURRENT.company'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: paramsCompany
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: 'addresses',
                                        let: {
                                            idaddress: '$$CURRENT.address'
                                        },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $and: [
                                                            { $eq: ["$_id", "$$idaddress"] }
                                                        ]
                                                    }
                                                }
                                            }
                                        ],
                                        as: 'idaddress'
                                    }
                                },
                                {
                                    $unwind: {
                                        "path": "$idaddress",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                }
                            ],
                            as: 'company'
                        }
                    },
                    {
                        $unwind: {
                            "path": "$company",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
                    {
                        $lookup: {
                            from: 'orders',
                            let: {
                                idorders: '$$CURRENT.orders'
                            },
                            pipeline: [
                                {
                                    $match: {
                                        $expr: {
                                            $and: paramsOrder
                                        }
                                    }
                                },
                                {
                                    $lookup: {
                                        from: 'trucks',
                                        let: {
                                            idtruck: '$$CURRENT.truck'
                                        },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $and: [
                                                            { $eq: ["$_id", "$$idtruck"] }
                                                        ]
                                                    }
                                                }
                                            },
                                            {
                                                $lookup: {
                                                    from: 'manufacturers',
                                                    let: {
                                                        idmanufacturer: '$$CURRENT.manufacturer'
                                                    },
                                                    pipeline: [
                                                        {
                                                            $match: {
                                                                $expr: {
                                                                    $and: [
                                                                        { $eq: ["$_id", "$$idmanufacturer"] }
                                                                    ]
                                                                }
                                                            }
                                                        }
                                                    ],
                                                    as: 'manufacturer'
                                                }
                                            },
                                            {
                                                $unwind: {
                                                    "path": "$manufacturer",
                                                    "preserveNullAndEmptyArrays": true
                                                }
                                            },
                                            {
                                                $lookup: {
                                                    from: 'models',
                                                    let: {
                                                        idmodel: '$$CURRENT.model'
                                                    },
                                                    pipeline: [
                                                        {
                                                            $match: {
                                                                $expr: {
                                                                    $and: [
                                                                        { $eq: ["$_id", "$$idmodel"] }
                                                                    ]
                                                                }
                                                            }
                                                        }
                                                    ],
                                                    as: 'model'
                                                }
                                            },
                                            {
                                                $unwind: {
                                                    "path": "$model",
                                                    "preserveNullAndEmptyArrays": true
                                                }
                                            },
                                        ],
                                        as: 'truck'
                                    }
                                },
                                {
                                    $unwind: {
                                        "path": "$truck",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                },
                                {
                                    $lookup: {
                                        from: 'companies',
                                        let: {
                                            idcompany: '$$CURRENT.company'
                                        },
                                        pipeline: [
                                            {
                                                $match: {
                                                    $expr: {
                                                        $and: paramsCompany
                                                    }
                                                }
                                            },
                                            {
                                                $lookup: {
                                                    from: 'addresses',
                                                    let: {
                                                        idaddress: '$$CURRENT.address'
                                                    },
                                                    pipeline: [
                                                        {
                                                            $match: {
                                                                $expr: {
                                                                    $and: [
                                                                        { $eq: ["$_id", "$$idaddress"] }
                                                                    ]
                                                                }
                                                            }
                                                        }
                                                    ],
                                                    as: 'idaddress'
                                                }
                                            },
                                            {
                                                $unwind: {
                                                    "path": "$idaddress",
                                                    "preserveNullAndEmptyArrays": true
                                                }
                                            }
                                        ],
                                        as: 'company'
                                    }
                                },
                                {
                                    $unwind: {
                                        "path": "$company",
                                        "preserveNullAndEmptyArrays": true
                                    }
                                }
                            ],
                            as: 'orders'
                        }
                    }
                )
            }



            // if (query.limit != null && query.skip != null) {
            //     search.push({
            //         $facet: {
            //             paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
            //             totalCount: [{
            //                 $count: 'count'
            //             }
            //             ]
            //         }
            //     });
            // }

            let obj;

            if (query.limit != null && query.skip != null && !query.isCount) {
                let skip = query.skip > 0 ? ((query.skip - 1) * query.limit) : 0;
                obj = await Collection.aggregate(search).skip(skip).limit(query.limit)
            } else {
                obj = await Collection.aggregate(search)
            }

            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listByFiltersCompany: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            let params = [];
            let paramsDate = [];
            let paramsOrder = [];
            let search = [];

            params.push({ status: 'ACTIVE' });

            params.push({ isSearchByCompany: true });

            if (query.company) {
                params.push({ 'company': mongoose.Types.ObjectId(query.company) });
            }

            try {//check if the date is valid
                if (query.beginDate) {
                    let date = new Date(query.beginDate);
                    date.toISOString();
                    date.setHours(0, 0, 0, 0);
                    query.beginDate = date;
                }
                if (query.endDate) {
                    let date = new Date(query.endDate);
                    date.toISOString();
                    date.setHours(23, 59, 59, 999);
                    query.endDate = date;
                }
            } catch (ex) {
            }

            if (query.beginDate) {
                paramsDate.push({ $gte: ["$dateOrder", new Date(query.beginDate)] });
            }
            if (query.endDate) {
                paramsDate.push({ $lte: ["$dateOrder", new Date(query.endDate)] });
            }

            if (query._id) {
                params.push({ _id: mongoose.Types.ObjectId(query._id) });
            }

            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }

            if (query.typeOrder) {
                params.push({ typeOrder: query.typeOrder });
            }

            if (query.order) {
                paramsOrder.push({ $in: [mongoose.Types.ObjectId(query.order), "$$idorders"] });
                params.push({ orders: { $ne: null } });
                params.push({ orders: { $ne: [] } });

            } else if (query.isHideEmptyOrders) {
                params.push({ orders: { $ne: null } });
                params.push({ orders: { $ne: [] } });
            }

            search = [
                {
                    $match: {
                        $expr: {
                            $and: paramsDate
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'manufacturers',
                        let: {
                            idmanufacturer: '$$CURRENT.manufacturer'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idmanufacturer"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'manufacturer'
                    }
                },
                {
                    $unwind: {
                        "path": "$manufacturer",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'models',
                        let: {
                            idtruckModel: '$$CURRENT.truckModel'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idtruckModel"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'truckModel'
                    }
                },
                {
                    $unwind: {
                        "path": "$truckModel",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'trucks',
                        let: {
                            idtruck: '$$CURRENT.truck'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idtruck"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idmanufacturer: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idmanufacturer"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$manufacturer",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                            {
                                $lookup: {
                                    from: 'models',
                                    let: {
                                        idmodel: '$$CURRENT.model'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idmodel"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'model'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$model",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                        ],
                        as: 'truck'
                    }
                },
                {
                    $unwind: {
                        "path": "$truck",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        let: {
                            idfamily: '$$CURRENT.family'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idfamily"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'family'
                    }
                },
                {
                    $unwind: {
                        "path": "$family",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'certifies',
                        let: {
                            idcertified: '$$CURRENT.certified'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idcertified"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'certified'
                    }
                },
                {
                    $unwind: {
                        "path": "$certified",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        let: {
                            idserviceFamilies: '$$CURRENT.serviceFamilies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idserviceFamilies"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'serviceFamilies'
                    }
                },
                {
                    $unwind: {
                        "path": "$serviceFamilies",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idsystemUser: '$$CURRENT.systemUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idsystemUser"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'company-users',
                                    let: {
                                        idcompanyUser: '$$CURRENT.companyUser'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idcompanyUser"] }
                                                    ]
                                                }
                                            }
                                        },
                                    ],
                                    as: 'companyUser'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$companyUser",
                                    "preserveNullAndEmptyArrays": true
                                }
                            }
                        ],
                        as: 'systemUser'
                    }
                },
                {
                    $unwind: {
                        "path": "$systemUser",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idtrucker: '$$CURRENT.trucker'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idtrucker"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'trucker'
                    }
                },
                {
                    $unwind: {
                        "path": "$trucker",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'orders',
                        let: {
                            idorders: '$$CURRENT.orders'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: paramsOrder
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'trucks',
                                    let: {
                                        idtruck: '$$CURRENT.truck'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idtruck"] }
                                                    ]
                                                }
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: 'manufacturers',
                                                let: {
                                                    idmanufacturer: '$$CURRENT.manufacturer'
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr: {
                                                                $and: [
                                                                    { $eq: ["$_id", "$$idmanufacturer"] }
                                                                ]
                                                            }
                                                        }
                                                    }
                                                ],
                                                as: 'manufacturer'
                                            }
                                        },
                                        {
                                            $unwind: {
                                                "path": "$manufacturer",
                                                "preserveNullAndEmptyArrays": true
                                            }
                                        },
                                        {
                                            $lookup: {
                                                from: 'models',
                                                let: {
                                                    idmodel: '$$CURRENT.model'
                                                },
                                                pipeline: [
                                                    {
                                                        $match: {
                                                            $expr: {
                                                                $and: [
                                                                    { $eq: ["$_id", "$$idmodel"] }
                                                                ]
                                                            }
                                                        }
                                                    }
                                                ],
                                                as: 'model'
                                            }
                                        },
                                        {
                                            $unwind: {
                                                "path": "$model",
                                                "preserveNullAndEmptyArrays": true
                                            }
                                        },
                                    ],
                                    as: 'truck'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$truck",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                            {
                                $lookup: {
                                    from: 'companies',
                                    let: {
                                        idCompany: '$$CURRENT.company'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idCompany"] }
                                                    ]
                                                }
                                            }
                                        },
                                    ],
                                    as: 'company'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$company",
                                    "preserveNullAndEmptyArrays": true
                                }
                            }
                        ],
                        as: 'orders'
                    }
                },
                {
                    $match: {
                        $and: params
                    }
                },
                { $sort: { dateOrder: -1 } }
            ]

            if (query.limit != null && query.skip != null) {
                search.push({
                    $facet: {
                        paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                        totalCount: [{
                            $count: 'count'
                        }
                        ]
                    }
                });
            }

            let obj = await Collection.aggregate(search)

            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listGroupByCertifie: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            let params = [];
            let search = [];


            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }

            params.push({ $in: ['$typeOrder', ['CERTIFIEDS']] });

            search = [
                {
                    $match: {
                        $expr: {
                            $and: params
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'certifies',
                        localField: 'certified',
                        foreignField: '_id',
                        as: 'certified'
                    }
                }, {
                    $unwind: {
                        path: "$certified",
                        preserveNullAndEmptyArrays: false
                    }
                },

                {
                    $group: {
                        _id: {
                            $dateToString: {
                                format: "%Y-%m",
                                date: "$dateOrder"
                            }
                        },
                        certifiedElement: { $push: { _id: "$certified" } }
                    },

                },
                {

                    $unwind: {
                        path: "$certifiedElement",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$certifiedElement",
                        certified: { $first: "$certifiedElement" },
                        date: { $first: "$_id" },
                        countOrders: { $sum: 1 }
                    },

                },
                {
                    $group: {
                        _id: "$date",
                        date: { $first: "$date" },
                        item: { $push: { certified: "$certified", countOrders: "$countOrders" } }
                    },

                },
                {
                    $project: {
                        _id: 0,
                        item: 1,
                        date: 1,
                    }
                },
                {
                    $sort: {
                        date: -1,
                    }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listGroupByService: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            let params = [];
            let search = [];


            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }


            params.push({ $in: ['$typeOrder', ['SERVICES']] });


            search = [
                {
                    $match: {
                        $expr: {
                            $and: params
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        localField: 'serviceFamilies',
                        foreignField: '_id',
                        as: 'service'
                    }
                }, {
                    $unwind: {
                        path: "$service",
                        preserveNullAndEmptyArrays: false
                    }
                },

                {
                    $group: {
                        _id: {
                            $dateToString: {
                                format: "%Y-%m",
                                date: "$dateOrder"
                            }
                        },
                        serviceElement: { $push: { _id: "$service" } }
                    },

                },
                {

                    $unwind: {
                        path: "$serviceElement",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$serviceElement",
                        service: { $first: "$serviceElement" },
                        date: { $first: "$_id" },
                        countOrders: { $sum: 1 }
                    },

                },
                {
                    $group: {
                        _id: "$date",
                        date: { $first: "$date" },
                        item: { $push: { service: "$service", countOrders: "$countOrders" } }
                    },

                },
                {
                    $project: {
                        _id: 0,
                        item: 1,
                        date: 1,
                    }
                },
                {
                    $sort: {
                        date: -1,
                    }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },

    listGroupByCertifie: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            let params = [];
            let search = [];


            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }

            params.push({ $in: ['$typeOrder', ['CERTIFIEDS']] });

            search = [
                {
                    $match: {
                        $expr: {
                            $and: params
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'certifies',
                        localField: 'certified',
                        foreignField: '_id',
                        as: 'certified'
                    }
                }, {
                    $unwind: {
                        path: "$certified",
                        preserveNullAndEmptyArrays: false
                    }
                },

                {
                    $group: {
                        _id: {
                            $dateToString: {
                                format: "%Y-%m",
                                date: "$dateOrder"
                            }
                        },
                        certifiedElement: { $push: { _id: "$certified" } }
                    },

                },
                {

                    $unwind: {
                        path: "$certifiedElement",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$certifiedElement",
                        certified: { $first: "$certifiedElement" },
                        date: { $first: "$_id" },
                        countOrders: { $sum: 1 }
                    },

                },
                {
                    $group: {
                        _id: "$date",
                        date: { $first: "$date" },
                        item: { $push: { certified: "$certified", countOrders: "$countOrders" } }
                    },

                },
                {
                    $project: {
                        _id: 0,
                        item: 1,
                        date: 1,
                    }
                },
                {
                    $sort: {
                        date: -1,
                    }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listGroupByService: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            let params = [];
            let search = [];


            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }


            params.push({ $in: ['$typeOrder', ['SERVICES']] });


            search = [
                {
                    $match: {
                        $expr: {
                            $and: params
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        localField: 'serviceFamilies',
                        foreignField: '_id',
                        as: 'service'
                    }
                }, {
                    $unwind: {
                        path: "$service",
                        preserveNullAndEmptyArrays: false
                    }
                },

                {
                    $group: {
                        _id: {
                            $dateToString: {
                                format: "%Y-%m",
                                date: "$dateOrder"
                            }
                        },
                        serviceElement: { $push: { _id: "$service" } }
                    },

                },
                {

                    $unwind: {
                        path: "$serviceElement",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$serviceElement",
                        service: { $first: "$serviceElement" },
                        date: { $first: "$_id" },
                        countOrders: { $sum: 1 }
                    },

                },
                {
                    $group: {
                        _id: "$date",
                        date: { $first: "$date" },
                        item: { $push: { service: "$service", countOrders: "$countOrders" } }
                    },

                },
                {
                    $project: {
                        _id: 0,
                        item: 1,
                        date: 1,
                    }
                },
                {
                    $sort: {
                        date: -1,
                    }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listGroupByFamily: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            let params = [];
            let search = [];


            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }

            if (query.typeOrder) {
                params.push({ $in: ['$typeOrder', query.typeOrder] });
            }
            else {
                params.push({ $in: ['$typeOrder', ['PARTS']] });
            }

            search = [
                {
                    $match: {
                        $expr: {
                            $and: params
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        localField: 'family',
                        foreignField: '_id',
                        as: 'family'
                    }
                }, {
                    $unwind: {
                        path: "$family",
                        preserveNullAndEmptyArrays: false
                    }
                },

                {
                    $group: {
                        _id: {
                            $dateToString: {
                                format: "%Y-%m",
                                date: "$dateOrder"
                            }
                        },
                        familyElement: { $push: { _id: "$family" } }
                    },

                },
                {

                    $unwind: {
                        path: "$familyElement",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$familyElement",
                        family: { $first: "$familyElement" },
                        date: { $first: "$_id" },
                        countOrders: { $sum: 1 }
                    },

                },
                {
                    $group: {
                        _id: "$date",
                        date: { $first: "$date" },
                        item: { $push: { family: "$family", countOrders: "$countOrders" } }
                    },

                },
                {
                    $project: {
                        _id: 0,
                        item: 1,
                        date: 1,
                    }
                },
                {
                    $sort: {
                        date: -1,
                    }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listGroupByManufacturer: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            console.log('#####busca')
            let params = [];
            let search = [];


            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }

            if (query.typeOrder) {
                params.push({ $in: ['$typeOrder', query.typeOrder] });
            }
            else {
                params.push({ $in: ['$typeOrder', ['PARTS']] });
            }

            search = [
                {
                    $match: {
                        $expr: {
                            $and: params
                        }
                    }
                },
                {
                    $lookup: {
                        from: 'manufacturers',
                        localField: 'manufacturer',
                        foreignField: '_id',
                        as: 'manufacturer'
                    }
                }, {
                    $unwind: {
                        path: "$manufacturer",
                        preserveNullAndEmptyArrays: false
                    }
                },

                {
                    $group: {
                        _id: {
                            $dateToString: {
                                format: "%Y-%m",
                                date: "$dateOrder"
                            }
                        },
                        manufacturerElement: { $push: { _id: "$manufacturer" } }
                    },

                },
                {

                    $unwind: {
                        path: "$manufacturerElement",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $group: {
                        _id: "$manufacturerElement",
                        manufacturer: { $first: "$manufacturerElement" },
                        date: { $first: "$_id" },
                        countOrders: { $sum: 1 }
                    },

                },
                {
                    $group: {
                        _id: "$date",
                        date: { $first: "$date" },
                        item: { $push: { manufacturer: "$manufacturer", countOrders: "$countOrders" } }
                    },

                },
                {
                    $project: {
                        _id: 0,
                        item: 1,
                        date: 1,
                    }
                },
                {
                    $sort: {
                        date: -1,
                    }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    },
    listGroupByDate: async (query) => {
        try {
            if (!isDbConnected) throw ({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
            console.log('#####busca')
            let params = [];
            let search = [];


            if (query.statusOrder) {
                params.push({ statusOrder: query.statusOrder });
            }

            if (query.typeOrder) {
                params.push({ $in: ['$typeOrder', query.typeOrder] });
            }

            search = [
                {
                    $match: {
                        $expr: {
                            $and: params
                        }
                    }
                },
                {
                    $match: {
                        "dateOrder": { $gte: new Date("2019-01-01"), $lt: new Date("2030-01-01") }
                    }
                },
                {
                    $group: {
                        _id: { $dateToString: { format: "%Y-%m", date: "$dateOrder" } },
                        nOrders: { $sum: 1 }
                    }
                },
                {
                    $sort: { _id: 1 }
                }
            ]
            let obj = await Collection.aggregate(search)
            return obj;
        } catch (error) {
            console.log(`caught the error: ${error}`);
            throw error;
        }
    }

}

module.exports = newModule;