const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

require("../models/Gasstation");
const Collection = mongoose.model('gasstations');

var newModule = {
    filter: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];

        if (query.status) {
            params.push('status',query.status);
        }else{
            params.push({status : 'ACTIVE'});
        }

        function diacriticSensitiveRegex(string = '') {
            return string.replace(/a/g, '[a,á,à,ä,ã,â]')
                .replace(/e/g, '[e,é,ë,ê]')
                .replace(/i/g, '[i,í,ï,î]')
                .replace(/c/g, '[c,ç]')
                .replace(/o/g, '[o,ó,ö,ò,õ,ô]')
                .replace(/u/g, '[u,ü,ú,ù,û]');
        }

        let paramsName = [];
        paramsName.push({
            $or: [{
                $or: [{
                    name: {
                        $regex: diacriticSensitiveRegex(query.name.toLowerCase()), $options: "i"
                    }
                }]
            }
            ]
        });

        search = [
            {
                $match: {
                    $and: paramsName
                }
            },
            {
                $match: {
                    $expr:{
                        $and: params
                    }
                }
            }
        ]
    
    
        Collection.aggregate (search)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;