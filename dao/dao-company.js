const mongoose = require('mongoose');

// Load Model
require('../models/SystemUsers');
require('../models/CompanyUsers');
require('../models/Companies');
require('../models/CustomerUsers');
require('../models/Addresses');
require('../models/ManufacturerFamilies');
require('../models/PartFamilies');
require('../models/Certified');
require('../models/Manufacturers');

const Collection = mongoose.model('companies');
const CollectionAddress = mongoose.model('addresses');

var newModule = {
    listAllByType: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let exclude = {
            password: 0
        }

        Collection.find({ typeService: { $in: query.typeService }, status: 'ACTIVE' }, exclude)
            .populate({
                path: 'address'
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    getLocations: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let exclude = {
            serviceFamilies: 0,
            certifies: 0,
            paymentType: 0,
            percentage: 0,
            date: 0,
            stateSubscription: 0,
            document: 0,
            isBranch: 0
        }

        Collection.find({ typeService: { $in: query.typeService }, status: 'ACTIVE' }, exclude[exclude])
            .populate({
                path: 'address'
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    loadById: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let exclude = {
            password: 0
        }

        Collection.find({ document: query.document }, exclude)
            .populate({
                path: 'address'
            })
            .populate({
                path: 'mainCompany'
            })
            .populate({
                path: 'serviceFamilies'
            })
            .populate({
                path: 'certifies'
            })
            .populate({
                path: 'manufacturerFamilies',
                populate: [
                    {
                        path: 'manufacturer'
                    },
                    {
                        path: 'families',
                    }
                ]
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    filterCompanies: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        let params = [];
        let pipelineCompany = [];

        let paramsCompany = [];
        // paramsCompany.push({ $eq: [ "$_id", "$$idCompany" ] });

        let paramsCompanyType = [];

        if (query.typeService) {
            paramsCompanyType.push({ $in: [query.typeService, '$typeService'] });
        }

        if (query.companyId) {
            params.push({ '_id': mongoose.Types.ObjectId(query.companyId) });
        }

        if (query.status) {
            params.push({ status: query.status });
        } else {
            params.push({ status: 'ACTIVE' });
        }


        function diacriticSensitiveRegex(string = '') {
            return string.replace(/a/g, '[a,á,à,ä,ã,â]')
                .replace(/e/g, '[e,é,ë,ê]')
                .replace(/i/g, '[i,í,ï,î]')
                .replace(/c/g, '[c,ç]')
                .replace(/o/g, '[o,ó,ö,ò,õ,ô]')
                .replace(/u/g, '[u,ü,ú,ù,û]');
        }

        let paramsName = [];
        paramsName.push({
            $or: [{
                $or: [{
                    fantasyName: {
                        $regex: diacriticSensitiveRegex(query.name.toLowerCase()), $options: "i"
                    }
                }]
            },
            {
                $or: [{
                    socialName: {
                        $regex: diacriticSensitiveRegex(query.name.toLowerCase()), $options: "i"
                    }
                }]
            }
            ]
        });

        pipelineCompany = [
            {
                $lookup: {
                    from: 'addresses',
                    let: {
                        idAddress: '$$CURRENT.address'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$_id", "$$idAddress"] },
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'address'
                }
            },
            {
                $unwind: "$address"
            },
            {
                $match: {
                    $expr: {
                        $and: paramsCompany
                    }
                }
            },
            {
                $addFields: {
                    typeService: {
                        $cond: {
                            if: {
                                $ne: [
                                    { $type: "$typeService" }, "array"
                                ]
                            },
                            then: [],
                            else: "$typeService"
                        }
                    }
                }
            },
            {
                $match: {
                    $expr: {
                        $and: paramsCompanyType
                    }
                }
            },
            {
                $match: {
                    $and: paramsName
                }
            },
            {
                $match: {
                    $and: params
                }
            }
        ];

        if (!query.isHideDetailsManufecturerFamily) {
            pipelineCompany.push(
                {
                    $lookup: {
                        from: 'manufacturer-families',
                        let: {
                            idsManufacturers: '$$CURRENT.manufacturerFamilies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $in: ['$_id', "$$idsManufacturers"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idManufacturer: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idManufacturer"] },
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind: '$manufacturer'
                            },
                            {
                                $lookup: {
                                    from: 'part-families',
                                    let: {
                                        idFamilies: '$$CURRENT.families'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $in: ['$_id', "$$idFamilies"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'families'
                                }
                            }
                        ],
                        as: 'manufacturerFamilies'
                    }
                }
            );
        }

        if (!query.isHideDetailsServiceFamilies) {
            pipelineCompany.push(
                {
                    $lookup: {
                        from: 'part-families',
                        let: {
                            idpartFamilies: '$$CURRENT.serviceFamilies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $in: ['$_id', "$$idpartFamilies"] }
                                        ]
                                    }
                                }
                            },

                        ],
                        as: 'serviceFamilies'
                    }
                }
            );
        }

        if (!query.isHideDetailsCertifies) {
            pipelineCompany.push(
                {
                    $lookup: {
                        from: 'certifies',
                        let: {
                            idcertifies: '$$CURRENT.certifies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $in: ['$_id', "$$idcertifies"] }
                                        ]
                                    }
                                }
                            },

                        ],
                        as: 'certifies'
                    }
                }
            );
        }

        if (query.limit != null && query.skip != null) {
            console.log('query.limit && query.skip');

            pipelineCompany.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                    totalCount: [{
                        $count: 'count'
                    }]
                }
            });
        }

        Collection.aggregate(pipelineCompany)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    },
    searchParts: (query, fncSuccess) => {

        let lookupAddress = getLookupAddress(query);
        let match = getMatchParts(query);

        Collection.aggregate([
            {
                $match: match[0]
            },
            {
                $match: {
                    $and: [
                        { status: 'ACTIVE' }
                    ]
                }
            },
            {
                $lookup: lookupAddress
            },
            {
                $unwind: '$address'
            },
            {
                $match: match[1]
            }
        ])
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    },
    search: (query, fncSuccess) => {

        let lookupAddress = getLookupAddress(query);
        let match = getMatch(query);
        let lookup = getLookup(query);

        let aggredatequery = [
            {
                $match: {
                    $and: [
                        { status: 'ACTIVE' }
                    ]
                }
            },
            {
                $lookup: lookupAddress
            },
            {
                $unwind: '$address'
            }
        ];
        if (match.length > 0) {
            aggredatequery.push(
                {
                    $match: match[0]
                },
                {
                    $match: match[1]
                }
            );
        }
        if (lookup.length > 0) {
            aggredatequery.push({
                $lookup: lookup
            });
        }
        console.log(JSON.stringify(aggredatequery));
        Collection.aggregate(aggredatequery)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    },
    searchV2: (query, fncSuccess) => {

        let aggredatequery = [
            {
                $geoNear: {
                    near: {
                        type: "Point",
                        coordinates: [query.longitude, query.latitude]
                    },
                    distanceField: "distance",
                    maxDistance: query.searchArea,
                    spherical: true
                }
            },
            {
                $lookup: {
                    from: 'companies',
                    localField: '_id',
                    foreignField: 'address',
                    as: 'company'
                }
            },
            {
                $unwind: {
                    path: "$company",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $match: {
                    $and: [
                        { 'company.status': 'ACTIVE' }
                    ]
                }
            }
        ];

        if (query.typeOrder == "PARTS") {

            if(query.manufacturer && query.family){

                aggredatequery.push(  
                    {
                        $lookup: {
                            from: 'manufacturer-families',
                            localField: 'company.manufacturerFamilies',
                            foreignField: '_id',
                            as: 'manufacturerFamilies'
                        }
                    },
                    {
                        $unwind: {
                            path: "$manufacturerFamilies",
                            preserveNullAndEmptyArrays: true
                        }
                    },
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$manufacturerFamilies.manufacturer', mongoose.Types.ObjectId(query.manufacturer)] },
                                    { $in: [mongoose.Types.ObjectId(query.family), '$manufacturerFamilies.families'] }
                                ]
                            }
                        }
                    }
                )
            }

            aggredatequery.push(
                {
                    $project: {
                        _id: "$company._id",
                        typeService: "$company.typeService",
                        "address.distance": "$$CURRENT.distance",
                    }
                },
                {
                    $project: {
                        _id: 1,
                        typeService: 1,
                        "hasPartsService": {
                            $cond: [{
                                $setIsSubset: [["PART_SUPPLIER"], "$typeService"]
                            }, true, false]
                        },
                        "address.distance": 1,
                    }
                },
                {
                    $match: {
                        $and: [
                            { _id: { $exists: true } },
                            { hasPartsService: true }
                        ]
                    }
                }
            );
        } else if (query.typeOrder == "SERVICES") {
            aggredatequery.push(
                {
                    $lookup: {
                        from: 'part-families',
                        localField: 'company.serviceFamilies',
                        foreignField: '_id',
                        as: 'serviceFamilies'
                    }
                },
                {
                    $unwind: {
                        path: "$serviceFamilies",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$serviceFamilies._id', mongoose.Types.ObjectId(query.service)] }
                            ]
                        }
                    }
                },
                {
                    $group: {
                        _id: "$company._id",
                        _idAddress: { $first: "$_id" },
                        company: { $first: "$company" },
                        address: { $first: "$$CURRENT" },
                    }
                },
                {
                    $project: {
                        _id: "$company._id",
                        // company: 1,
                        "address.distance": "$address.distance",
                    }
                },
                {
                    $match: {
                        $and: [
                            { _id: { $exists: true } },
                        ]
                    }
                }
            );
        } else if (query.typeOrder == "CERTIFIEDS") {
            aggredatequery.push(
                {
                    $lookup: {
                        from: 'certifies',
                        localField: 'company.certifies',
                        foreignField: '_id',
                        as: 'certifies'
                    }
                },
                {
                    $unwind: {
                        path: "$certifies",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    $match: {
                        $expr: {
                            $and: [
                                { $eq: ['$certifies._id', mongoose.Types.ObjectId(query.certified)] }
                            ]
                        }
                    }
                },
                {
                    $group: {
                        _id: "$company._id",
                        _idAddress: { $first: "$_id" },
                        company: { $first: "$company" },
                        address: { $first: "$$CURRENT" },
                    }
                },
                {
                    $project: {
                        _id: "$company._id",
                        // company: 1,
                        "address.distance": "$address.distance",
                    }
                },
                {
                    $match: {
                        $and: [
                            { _id: { $exists: true } },
                        ]
                    }
                }
            );
        }

        console.log(JSON.stringify(aggredatequery));

        CollectionAddress.aggregate(aggredatequery)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err.stack}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    },
    listAllActive: (fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let returnedFields = JSON.parse(JSON.stringify({
            fantasyName: 1,
            socialName: 1,
            email: 1,
            document: 1,
            primaryPhone: 1,
            address: 1
        }))
        Collection.find({ status: 'ACTIVE' }, returnedFields)
            .populate('address','state cep city address number' )
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    }
};

const getLookupAddress = (query) => {
    return {
        from: 'addresses',
        let: {
            idAddress: '$$CURRENT.address'
        },
        pipeline: [
            {
                $geoNear: {
                    near: {
                        type: "Point",
                        coordinates: [query.longitude, query.latitude]
                    },
                    distanceField: "distance",
                    maxDistance: query.searchArea,
                    spherical: true

                }
            },
            {
                $match: {
                    $expr: {
                        $and: [
                            { $eq: ['$_id', '$$idAddress'] },
                            { $eq: ['$status', 'ACTIVE'] }
                        ]
                    }
                }
            }
        ],
        as: 'address'
    };
}

const getMatchParts = (query) => {
    let match = [];

    match.push({
        address: { $type: 'objectId' }
    });
    match.push({
        address: { $exists: true, $ne: [] }
    });
    return match;
}


const getMatch = (query) => {
    let match = [];

    if (query.typeOrder == 'PARTS') {
        // match.push({
        //     manufacturerFamilies: { $type: 'array' },
        //     address: { $type: 'objectId' }
        // });
        // match.push({
        //     manufacturerFamilies: { $exists: true, $ne: [] },
        //     address: { $exists: true, $ne: [] }
        // });
    } else if (query.typeOrder == 'SERVICES') {
        match.push({
            serviceFamilies: { $type: 'array' },
            address: { $type: 'objectId' }
        });
        match.push({
            serviceFamilies: { $exists: true, $ne: [] },
            address: { $exists: true, $ne: [] }
        });
    } else if (query.typeOrder == 'CERTIFIEDS') {
        match.push({
            certifies: { $type: 'array' },
            address: { $type: 'objectId' }
        });
        match.push({
            certifies: { $exists: true, $ne: [] },
            address: { $exists: true, $ne: [] }
        });
    }

    return match;
}

const getLookup = (query) => {
    let lookup = {};

    if (query.typeOrder == 'PARTS') {
        // let params = [];
        // params.push({ $in: ['$_id', '$$idsManufacturerFamilies'] });
        // if (query.manufacturer) {
        //     params.push({ $eq: ['$manufacturer', mongoose.Types.ObjectId(query.manufacturer)] });
        // }
        // if (query.family) {
        //     params.push({ $in: [mongoose.Types.ObjectId(query.family), '$families'] });
        // }
        // if (query.truckType) {
        //     params.push({ $eq: ['$type', query.truckType] });
        // }
        // params.push({ status: 'ACTIVE' });
        // lookup = {
        //     from: 'manufacturer-families',
        //     let: {
        //         idsManufacturerFamilies: '$$CURRENT.manufacturerFamilies'
        //     },
        //     pipeline: [
        //         {
        //             $match: {
        //                 $expr: {
        //                     $and: params
        //                 }
        //             }
        //         }
        //     ],
        //     as: 'manufacturerFamilies'
        // };
    } else if (query.typeOrder == 'SERVICES') {
        lookup = {
            from: 'part-families',
            let: {
                idsServiceFamilies: '$$CURRENT.serviceFamilies'
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                { $in: ['$_id', '$$idsServiceFamilies'] },
                                { $eq: ['$_id', mongoose.Types.ObjectId(query.service)] },
                                { $eq: ['$status', 'ACTIVE'] }
                            ]
                        }
                    }
                }
            ],
            as: 'serviceFamilies'
        };
    } else if (query.typeOrder == 'CERTIFIEDS') {
        lookup = {
            from: 'certifies',
            let: {
                idsCertifies: '$$CURRENT.certifies'
            },
            pipeline: [
                {
                    $match: {
                        $expr: {
                            $and: [
                                { $in: ['$_id', '$$idsCertifies'] },
                                { $eq: ['$_id', mongoose.Types.ObjectId(query.certified)] },
                                { $eq: ['$status', 'ACTIVE'] }
                            ]
                        }
                    }
                }
            ],
            as: 'certifies'
        };
    }

    return lookup;
}

module.exports = newModule;