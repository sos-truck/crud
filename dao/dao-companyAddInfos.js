const mongoose = require('mongoose');

// Load Model
require('../models/CompanyAddicionalInfos');

const Collection = mongoose.model('company-add-infos');

var newModule = {
    deleteByCompany: async (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            let response = await Collection.deleteMany({company: element.company});
            if (fncSuccess)
                return fncSuccess(response);

            return response;    
        } catch (err) {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            if (fncError)
                return fncError(err.stack);

            return err.stack;
        }
    },
    findByCompany: async (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            let response = await Collection.findOne({company: element.company});
            if (fncSuccess)
                return fncSuccess(params);

            return response;
        } catch (err) {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            if (fncError)
                return fncError(err.stack);

            return err.stack;
        }
    }
};

module.exports = newModule;