const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

// Load Model
require('../models/ChatMessages');
require('../models/SystemUsers');
require('../models/CompanyUsers');
require('../models/CustomerUsers');
require('../models/Companies');

const Collection = mongoose.model('chat-messages');

var newModule = {
    filterWithoutLookup: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let search = [];

        params.push({ status: 'ACTIVE' });

        if (query.trucker){
            params.push({ 'trucker': mongoose.Types.ObjectId(query.trucker) });
        }
        if (query.company) {
            params.push({ 'company': mongoose.Types.ObjectId(query.company) });
        }
        if (query.order){
            params.push({ 'order': mongoose.Types.ObjectId(query.order) });
        }
    

        search = [
            {
                $match: {
                    $and: params
                }
            }
        ]

        if (query.limit!=null && query.skip!=null){
            search.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                        totalCount: [{
                            $count: 'count'
                        }
                    ]
                }
            });
        }
    
        Collection.aggregate (search)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    },
    filter: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];
        let search = [];
        let paramsSystemUsersFrom = [];
        let paramsSystemUsersTo = [];

        paramsSystemUsersTo.push({ $eq: [ "$_id", "$$idto" ] });
        paramsSystemUsersFrom.push({ $eq: [ "$_id", "$$idfrom" ] });
        
        params.push({status : 'ACTIVE'})
        
        if (query.from){
            paramsSystemUsersFrom.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.from) ] });
        }
        if (query.to){
            paramsSystemUsersTo.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.to) ] });
        }
        if (query.order){
            params.push({ $eq: [ "$_id", mongoose.Types.ObjectId(query.order) ] });
        }
        if (query.conversationId){
            params.push({ conversationId : query.conversationId });
        }

        search = [
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idfrom: '$$CURRENT.from'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: paramsSystemUsersFrom
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: 'company-users',
                                let: {
                                    idcompanyUser: '$$CURRENT.companyUser'
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr:{
                                                $and: { $eq: [ "$_id", "$$idcompanyUser" ] }
                                            }
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: 'companies',
                                            let: {
                                                idcompany: '$$CURRENT.company'
                                            },
                                            pipeline: [
                                                {
                                                    $match: {
                                                        $expr:{
                                                            $and: { $eq: [ "$_id", "$$idcompany" ] }
                                                        }
                                                    }
                                                }
                                            ],
                                            as: 'company'
                                        }
                                    },
                                    {
                                        $unwind: {
                                            "path": "$company",
                                            "preserveNullAndEmptyArrays": true
                                        }
                                    }
                                ],
                                as: 'companyUser'
                            }
                        },
                        {
                            $unwind: {
                                "path": "$companyUser",
                                "preserveNullAndEmptyArrays": true
                            }
                        },
                        {
                            $lookup: {
                                from: 'customer-users',
                                let: {
                                    idcustomerUser: '$$CURRENT.customerUser'
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr:{
                                                $and: { $eq: [ "$_id", "$$idcustomerUser" ] }
                                            }
                                        }
                                    }
                                ],
                                as: 'customerUser'
                            }
                        },
                        {
                            $unwind: {
                                "path": "$customerUser",
                                "preserveNullAndEmptyArrays": true
                            }
                        }
                    ],
                    as: 'from'
                }
            },
            {
                $unwind: {
                    "path": "$from",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idto: '$$CURRENT.to'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: paramsSystemUsersTo
                                }
                            }
                        },
                        {
                            $lookup: {
                                from: 'company-users',
                                let: {
                                    idcompanyUser: '$$CURRENT.companyUser'
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr:{
                                                $and: { $eq: [ "$_id", "$$idcompanyUser" ] }
                                            }
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: 'companies',
                                            let: {
                                                idcompany: '$$CURRENT.company'
                                            },
                                            pipeline: [
                                                {
                                                    $match: {
                                                        $expr:{
                                                            $and: { $eq: [ "$_id", "$$idcompany" ] }
                                                        }
                                                    }
                                                }
                                            ],
                                            as: 'company'
                                        }
                                    },
                                    {
                                        $unwind: {
                                            "path": "$company",
                                            "preserveNullAndEmptyArrays": true
                                        }
                                    }
                                ],
                                as: 'companyUser'
                            }
                        },
                        {
                            $unwind: {
                                "path": "$companyUser",
                                "preserveNullAndEmptyArrays": true
                            }
                        },
                        {
                            $lookup: {
                                from: 'customer-users',
                                let: {
                                    idcustomerUser: '$$CURRENT.customerUser'
                                },
                                pipeline: [
                                    {
                                        $match: {
                                            $expr:{
                                                $and: { $eq: [ "$_id", "$$idcustomerUser" ] }
                                            }
                                        }
                                    }
                                ],
                                as: 'customerUser'
                            }
                        },
                        {
                            $unwind: {
                                "path": "$customerUser",
                                "preserveNullAndEmptyArrays": true
                            }
                        }
                    ],
                    as: 'to'
                }
            },
            {
                $unwind: {
                    "path": "$to",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                $lookup: {
                    from: 'orders',
                    let: {
                        idOrder: '$$CURRENT.order'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: { $eq: [ "$_id", "$$idOrder" ] }
                                }
                            }
                        }
                    ],
                    as: 'order'
                }
            },
            {
                $unwind: {
                    "path": "$order",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $match: { 
                    $expr:{ 
                        $and: params
                    }
                }
            }
        ]


        if (query.isGroupConversation){
            search.push( { 
                $group : { 
                    _id : "$from._id",
                    date: { $last: "$date" },
                    msg: { $last: "$msg" },
                    order: { $last: "$order" },
                    conversationId: { $last: "$conversationId" },
                    from: { $last: "$from" },
                    to: { $last: "$to" },
                } 
            });

            search.push({
                $sort: { date: -1}
            });
        }else{
            search.push({
                $sort: { date: 1}
            });
        }

        if (query.limit!=null && query.skip!=null){
            search.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                        totalCount: [{
                            $count: 'count'
                        }
                    ]
                }
            });
        }

        
    
        Collection.aggregate (search)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;