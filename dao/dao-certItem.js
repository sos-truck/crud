const mongoose = require('mongoose');

// Load Model
require('../models/CertifiedItem');

const Collection = mongoose.model('certified-items');

var newModule = {
    deleteCertByIds: async (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            let response = await Collection.deleteMany({ _id: { $in: element._ids } });
            if (fncSuccess)
                return fncSuccess(response);

            return response;    
        } catch (err) {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            if (fncError)
                return fncError(err.stack);

            return err.stack;
        }
    }
};

module.exports = newModule;