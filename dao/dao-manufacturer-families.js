const mongoose = require('mongoose');

// Load Model
require('../models/ManufacturerFamilies');

const Collection = mongoose.model('manufacturer-families');

var newModule = {
    deleteByCompany: async (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        try {
            let response = await Collection.deleteMany({company: element.company});
            if (fncSuccess)
                return fncSuccess(response);

            return response;    
        } catch (err) {
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            console.log(`caught the error: ${err.stack}`);
            if (fncError)
                return fncError(err.stack);

            return err.stack;
        }
    }
};

module.exports = newModule;