const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

// Load Model
require('../models/Orders');
require('../models/Trucks');
require('../models/SystemUsers');
require('../models/CompanyUsers');
require('../models/PartFamilies');
require('../models/Manufacturers');
require('../models/Models');
require('../models/Companies');
require('../models/Addresses');

const Collection = mongoose.model('orders');

var newModule = {
    getByHashPayment: async (element) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        try {
            let response = await Collection.findOne({ 'paymentObject.hash': element.hash_codes })
            return response;
        } catch (err) {
            console.log(`caught the error: ${err.stack}`);

            throw err;
        }
    },
    findById: async (element) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        try {
            let response = await Collection.findOne({ _id: element._id })
                .populate({
                    path: 'manufacturer'
                })
                .populate({
                    path: 'family'
                })
                .populate({
                    path: 'certified'
                })
                .populate({
                    path: 'truckModel'
                })
                .populate({
                    path: 'serviceFamilies'
                })
                .populate({
                    path: 'truck',
                    populate: {
                        path: 'manufacturer'
                    },
                    populate: {
                        path: 'model'
                    }
                })
                .populate({
                    path: 'company',
                    populate: {
                        path: 'address'
                    }
                });

            return response;
        } catch (err) {
            console.log(`caught the error: ${err.stack}`);

            throw err;
        }
    },
    changeStatusElements: async (element) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        try {
            let response = await Collection.updateMany({ orderHolder: element.orderHolder }, { $set: { statusOrder: element.statusOrder, justifyClose: element.justifyClose } });
            return response;
        } catch (err) {
            console.log(`caught the error: ${err.stack}`);
            throw err;
        }
    },
    listByFilters: (query, fncSuccess, fncError) => {

        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        let params = [];
        let paramsDate = [];
        let paramsCompany = [];
        let paramsUser = [];
        let search = [];

        paramsCompany.push({ $eq: ["$_id", "$$idcompany"] });
        paramsUser.push({ $eq: ["$_id", "$$idsystemUser"] });

        params.push({ statusOrder: { $exists: true } });

        try {//check if the date is valid
            if (query.beginDate) {
                let date = new Date(query.beginDate);
                date.toISOString();
                date.setHours(0, 0, 0, 0);
                query.beginDate = date;
            }
            if (query.endDate) {
                let date = new Date(query.endDate);
                date.toISOString();
                date.setHours(23, 59, 59, 999);
                query.endDate = date;
            }
        } catch (ex) {
        }

        if (query.beginDate) {
            paramsDate.push({ $gte: ["$dateOrder", new Date(query.beginDate)] });
        }

        if (query.endDate) {
            paramsDate.push({ $lte: ["$dateOrder", new Date(query.endDate)] });
        }

        if (query.statusOrder) {
            params.push({ statusOrder: query.statusOrder });
        }

        if (query.companyRequest) {
            params.push({ companyRequest: mongoose.Types.ObjectId(query.companyRequest) });
        }

        if (query.company) {
            params.push({ company: mongoose.Types.ObjectId(query.company) });
        }

        if (query.typeOrder) {
            params.push({ typeOrder: query.typeOrder });
        }

        if (query.systemUser) {
            paramsUser.push({ $eq: ["$_id", mongoose.Types.ObjectId(query.systemUser)] });
        }

        search = [
            { $sort: { dateOrder: -1 } },
            {
                $match: {
                    $and: params
                }
            },
            {
                $match: {
                    $expr: {
                        $and: paramsDate
                    }
                }
            }
        ]

        if (query.isCount) {
            search.push({
                $group: {
                    _id: null,
                    count: {
                        $sum: 1
                    }
                }
            });
        } else {
            search.push(
                {
                    $lookup: {
                        from: 'manufacturers',
                        let: {
                            idmanufacturer: '$$CURRENT.manufacturer'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idmanufacturer"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'manufacturer'
                    }
                },
                {
                    $unwind: {
                        "path": "$manufacturer",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'models',
                        let: {
                            idmodel: '$$CURRENT.truckModel'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idmodel"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'truckModel'
                    }
                },
                {
                    $unwind: {
                        "path": "$truckModel",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        let: {
                            idfamily: '$$CURRENT.family'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idfamily"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'family'
                    }
                },
                {
                    $unwind: {
                        "path": "$family",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'trucks',
                        let: {
                            idtruck: '$$CURRENT.truck'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idtruck"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idmanufacturer: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idmanufacturer"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$manufacturer",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                            {
                                $lookup: {
                                    from: 'models',
                                    let: {
                                        idmodel: '$$CURRENT.model'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idmodel"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'model'
                                }
                            },
                            {
                                $unwind: {
                                    "path": "$model",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },
                        ],
                        as: 'truck'
                    }
                },
                {
                    $unwind: {
                        "path": "$truck",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'certifies',
                        let: {
                            idcertified: '$$CURRENT.certified'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idcertified"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'certified'
                    }
                },
                {
                    $unwind: {
                        "path": "$certified",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'part-families',
                        let: {
                            idserviceFamilies: '$$CURRENT.serviceFamilies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idserviceFamilies"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'serviceFamilies'
                    }
                },
                {
                    $unwind: {
                        "path": "$serviceFamilies",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idsystemUser: '$$CURRENT.systemUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: paramsUser
                                    }
                                }
                            }
                        ],
                        as: 'systemUser'
                    }
                },
                {
                    $unwind: {
                        "path": "$systemUser",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'system-users',
                        let: {
                            idtrucker: '$$CURRENT.trucker'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $eq: ["$_id", "$$idtrucker"] }
                                        ]
                                    }
                                }
                            }
                        ],
                        as: 'trucker'
                    }
                },
                {
                    $unwind: {
                        "path": "$trucker",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: 'companies',
                        let: {
                            idcompany: '$$CURRENT.company'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: paramsCompany
                                    }
                                }
                            },
                            // {
                            //     $lookup: {
                            //         from: 'addresses',
                            //         let: {
                            //             idaddress: '$$CURRENT.address'
                            //         },
                            //         pipeline: [
                            //             {
                            //                 $match: {
                            //                     $expr: {
                            //                         $and: [
                            //                             { $eq: ["$_id", "$$idaddress"] }
                            //                         ]
                            //                     }
                            //                 }
                            //             }
                            //         ],
                            //         as: 'idaddress'
                            //     }
                            // },
                            // {
                            //     $unwind: {
                            //         "path": "$idaddress",
                            //         "preserveNullAndEmptyArrays": true
                            //     }
                            // }
                        ],
                        as: 'company'
                    }
                },
                {
                    $unwind: {
                        "path": "$company",
                        "preserveNullAndEmptyArrays": false
                    }
                });
        }

        // if (query.limit != null && query.skip != null) {
        //     search.push({
        //         $facet: {
        //             paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
        //             totalCount: [{
        //                 $count: 'count'
        //             }
        //             ]
        //         }
        //     });
        // }

        console.log(JSON.stringify(search));

        if (query.limit != null && query.skip != null && !query.isCount) {
            let skip = query.skip > 0 ? ((query.skip - 1) * query.limit) : 0;
            Collection.aggregate(search).skip(skip).limit(query.limit)
                .then(obj => {
                    fncSuccess(obj);
                }).catch(err => {
                    console.log(`caught the error: ${err}`);
                    if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                        return fncSuccess(null);
                    }
                    fncSuccess(err.stack);
                });
        } else {
            Collection.aggregate(search)
                .then(obj => {
                    fncSuccess(obj);
                }).catch(err => {
                    console.log(`caught the error: ${err}`);
                    if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                        return fncSuccess(null);
                    }
                    fncSuccess(err.stack);
                });
        }
    },
    listByFilters2: (query, fncSuccess, fncError) => {

        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        let params = [];
        let paramsDate = [];
        let paramsCompany = [];
        let paramsUser = [];
        let search = [];

        paramsCompany.push({ $eq: ["$_id", "$$idcompany"] });


        params.push({ statusOrder: { $exists: true } });

        try {//check if the date is valid
            if (query.beginDate) {
                let date = new Date(query.beginDate);
                date.toISOString();
                date.setHours(0, 0, 0, 0);
                query.beginDate = date;
            }
            if (query.endDate) {
                let date = new Date(query.endDate);
                date.toISOString();
                date.setHours(23, 59, 59, 999);
                query.endDate = date;
            }
        } catch (ex) {
        }

        if (query.beginDate) {
            paramsDate.push({ $gte: ["$dateOrder", new Date(query.beginDate)] });
        }

        if (query.endDate) {
            paramsDate.push({ $lte: ["$dateOrder", new Date(query.endDate)] });
        }

        if (query.statusOrder) {
            params.push({ statusOrder: query.statusOrder });
        }

        if (query.company) {
            params.push({  company:  mongoose.Types.ObjectId(query.company) });
        }

        if (query.typeOrder) {
            params.push({ typeOrder: query.typeOrder });
        }



        search = [
            {
                $match: {
                    $expr: {
                        $and: paramsDate
                    }
                }
            },
            {
                $lookup: {
                    from: 'manufacturers',
                    let: {
                        idmanufacturer: '$$CURRENT.manufacturer'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$_id", "$$idmanufacturer"] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'manufacturer'
                }
            },
            {
                $unwind: {
                    "path": "$manufacturer",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: 'models',
                    let: {
                        idmodel: '$$CURRENT.truckModel'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: [
                                        { $eq: [ "$_id", "$$idmodel" ] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'truckModel'
                }
            },
            {
                $unwind:{
                    "path": "$truckModel",
                    "preserveNullAndEmptyArrays": true
                }
            },
            // {
            //     $lookup: {
            //         from: 'part-families',
            //         let: {
            //             idfamily: '$$CURRENT.family'
            //         },
            //         pipeline: [
            //             {
            //                 $match: {
            //                     $expr: {
            //                         $and: [
            //                             { $eq: ["$_id", "$$idfamily"] }
            //                         ]
            //                     }
            //                 }
            //             }
            //         ],
            //         as: 'family'
            //     }
            // },
            // {
            //     $unwind: {
            //         "path": "$family",
            //         "preserveNullAndEmptyArrays": true
            //     }
            // },
            // {
            //     $lookup: {
            //         from: 'trucks',
            //         let: {
            //             idtruck: '$$CURRENT.truck'
            //         },
            //         pipeline: [
            //             {
            //                 $match: {
            //                     $expr: {
            //                         $and: [
            //                             { $eq: ["$_id", "$$idtruck"] }
            //                         ]
            //                     }
            //                 }
            //             },
            //             {
            //                 $lookup: {
            //                     from: 'manufacturers',
            //                     let: {
            //                         idmanufacturer: '$$CURRENT.manufacturer'
            //                     },
            //                     pipeline: [
            //                         {
            //                             $match: {
            //                                 $expr: {
            //                                     $and: [
            //                                         { $eq: ["$_id", "$$idmanufacturer"] }
            //                                     ]
            //                                 }
            //                             }
            //                         }
            //                     ],
            //                     as: 'manufacturer'
            //                 }
            //             },
            //             {
            //                 $unwind: {
            //                     "path": "$manufacturer",
            //                     "preserveNullAndEmptyArrays": true
            //                 }
            //             },
            //             {
            //                 $lookup: {
            //                     from: 'models',
            //                     let: {
            //                         idmodel: '$$CURRENT.model'
            //                     },
            //                     pipeline: [
            //                         {
            //                             $match: {
            //                                 $expr: {
            //                                     $and: [
            //                                         { $eq: ["$_id", "$$idmodel"] }
            //                                     ]
            //                                 }
            //                             }
            //                         }
            //                     ],
            //                     as: 'model'
            //                 }
            //             },
            //             {
            //                 $unwind: {
            //                     "path": "$model",
            //                     "preserveNullAndEmptyArrays": true
            //                 }
            //             },
            //         ],
            //         as: 'truck'
            //     }
            // },
            // {
            //     $unwind: {
            //         "path": "$truck",
            //         "preserveNullAndEmptyArrays": true
            //     }
            // },
            // {
            //     $lookup: {
            //         from: 'certifies',
            //         let: {
            //             idcertified: '$$CURRENT.certified'
            //         },
            //         pipeline: [
            //             {
            //                 $match: {
            //                     $expr: {
            //                         $and: [
            //                             { $eq: ["$_id", "$$idcertified"] }
            //                         ]
            //                     }
            //                 }
            //             }
            //         ],
            //         as: 'certified'
            //     }
            // },
            // {
            //     $unwind: {
            //         "path": "$certified",
            //         "preserveNullAndEmptyArrays": true
            //     }
            // },
            // {
            //     $lookup: {
            //         from: 'part-families',
            //         let: {
            //             idserviceFamilies: '$$CURRENT.serviceFamilies'
            //         },
            //         pipeline: [
            //             {
            //                 $match: {
            //                     $expr: {
            //                         $and: [
            //                             { $eq: ["$_id", "$$idserviceFamilies"] }
            //                         ]
            //                     }
            //                 }
            //             }
            //         ],
            //         as: 'serviceFamilies'
            //     }
            // },
            // {
            //     $unwind: {
            //         "path": "$serviceFamilies",
            //         "preserveNullAndEmptyArrays": true
            //     }
            // },
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idsystemUser: '$$CURRENT.systemUser'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: paramsUser
                                }
                            }
                        }
                    ],
                    as: 'systemUser'
                }
            },
            {
                $unwind: {
                    "path": "$systemUser",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idtrucker: '$$CURRENT.trucker'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$_id", "$$idtrucker"] }
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'trucker'
                }
            },
            {
                $unwind: {
                    "path": "$trucker",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: 'companies',
                    let: {
                        idcompany: '$$CURRENT.company'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: paramsCompany
                                }
                            }
                        },
                        // {
                        //     $lookup: {
                        //         from: 'addresses',
                        //         let: {
                        //             idaddress: '$$CURRENT.address'
                        //         },
                        //         pipeline: [
                        //             {
                        //                 $match: {
                        //                     $expr: {
                        //                         $and: [
                        //                             { $eq: ["$_id", "$$idaddress"] }
                        //                         ]
                        //                     }
                        //                 }
                        //             }
                        //         ],
                        //         as: 'idaddress'
                        //     }
                        // },
                        // {
                        //     $unwind: {
                        //         "path": "$idaddress",
                        //         "preserveNullAndEmptyArrays": true
                        //     }
                        // }
                    ],
                    as: 'company'
                }
            },
            {
                $unwind: {
                    "path": "$company",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $match: {
                    $and: params
                }
            },
            {
                $project: {
                    systemUser: "$systemUser",
                    company: "$company",
                    manufacturer:"$manufacturer",
                    truckModel: "$truckModel",
                    trucker: "$trucker",
                    certified: "$certified",
                    isSearchByCompany: "$isSearchByCompany",
                    statusOrder: "$statusOrder",
                    description: "$description",
                    typeOrder: "$typeOrder",
                    latitude: "$latitude",
                    longitude: "$longitude",
                    dateOrder: "$dateOrder",
                    distance: "$distance"

                }    
            },
            { $sort: { dateOrder: -1 } }
        ]

        if (query.limit != null && query.skip != null) {
            search.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                    totalCount: [{
                        $count: 'count'
                    }
                    ]
                }
            });
        }

        console.log(JSON.stringify(search));

        Collection.aggregate(search)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    },
    listCompaniesOrderPerDay: (query, fncSuccess, fncError) => {

        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        let params = [];
        let paramsDate = [];
        let paramsCompany = [];
        let paramsUser = [];
        let search = [];

        paramsCompany.push({ $eq: ["$_id", "$$idcompany"] });
        params.push({ statusOrder: { $exists: true } });

        let date = new Date();
        date.toLocaleString();
        date.setHours(0, 0, 0, 0);
        let beginDate = date;

        let dateEnd = new Date();
        dateEnd.toLocaleString();
        dateEnd.setHours(23, 59, 59, 999);
        let endDate = dateEnd;

        paramsDate.push({ $gte: ["$dateOrder", new Date(beginDate)] });
        paramsDate.push({ $lte: ["$dateOrder", new Date(endDate)] });


        search = [
            {
                $match: {
                    $expr: {
                        $and: paramsDate
                    }
                }
            },
            {
                $lookup: {
                    from: 'companies',
                    let: {
                        idcompany: '$$CURRENT.company'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: paramsCompany
                                }
                            }
                        },
                    ],
                    as: 'company'
                }
            },
            {
                $unwind: {
                    "path": "$company",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                $match: {
                    $and: params
                }
            },
            {
                $project: {
                    _id: '$company._id',
                    fantasyName: '$company.fantasyName',
                    email: '$company.email',
                    primaryPhone: '$company.primaryPhone'
                }
            },
            { $sort: { dateOrder: -1 } }
        ]

        Collection.aggregate(search)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    }
};

module.exports = newModule;