const mongoose = require('mongoose');

// Load Model
require('../models/SystemUsers');
require('../models/Trucks');
require('../models/CompanyUsers');
require('../models/Companies');
require('../models/CustomerUsers');
require('../models/Addresses');
require('../models/ManufacturerFamilies');
require('../models/Manufacturers');
require('../models/PartFamilies');
require('../models/Curriculums');
require('../models/Healths');

const Collection = mongoose.model('system-users');
var newModule = {

    listCustomerWhithoutTruck: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        let params = [];
        let search = [];


        if (query.status) {
            params.push({ status: query.status });
        } else {
            params.push({ status: 'ACTIVE' });
        }

        search = [
            {
                $lookup: {
                    from: 'trucks',
                    let: {
                        idSystemUser: '$$CURRENT._id'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$systemUser", "$$idSystemUser"] },
                                    ]
                                }
                            }
                        },

                    ],
                    as: 'truck'
                }
            },

            {
                $lookup: {
                    from: 'customer-users',
                    let: {
                        idCustomerUser: '$$CURRENT.customerUser'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$_id", "$$idCustomerUser"] },
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'customerUser'
                }
            },
            {
                $unwind: "$customerUser"
            },
            {
                $match: {
                    companyUser: { $exists: false }
                }
            },
            {
                $match: {
                    status: "ACTIVE"
                }
            },
            {
                $match: {
                    type: "USER-CUSTOMER"
                }
            },
            {
                $match: {
                    truck: []
                }
            },
        ]

        if (query.limit != null && query.skip != null) {
            search.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                    totalCount: [{
                        $count: 'count'
                    }
                    ]
                }
            });
        }

        Collection.aggregate(search)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });

    },
    findByPhone: (element, excludedKeysOnResult, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let excluded = {};
        excludedKeysOnResult.forEach(function (value) {
            excluded[value] = 0;
        });
        Collection.findOne({ primaryPhone: element.primaryPhone })
            .populate({
                path: 'customerUser',
                populate: [
                    {
                        path: 'curriculum',
                        select: '-__v'
                    },
                    {
                        path: 'health',
                        select: '-__v'
                    }
                ]
            })
            .populate({
                path: 'companyUser',
                populate: {
                    path: 'company',
                    select: '-status -__v -families -manufacturers -categories -models',
                    populate: [
                        {
                            path: 'manufacturerFamilies',
                            select: '-status -__v -company',
                            populate: [
                                {
                                    path: 'families',
                                    select: '-status -__v'
                                },
                                {
                                    path: 'manufacturer',
                                    select: '-status -__v'
                                }
                            ]
                        },
                        {
                            path: 'address',
                            select: '-status -__v'
                        }
                    ]
                }
            })
            .populate({
                path: 'address'
            })
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    },
    findByNetworkId: (element, excludedKeysOnResult, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let excluded = {};
        excludedKeysOnResult.forEach(function (value) {
            excluded[value] = 0;
        });
        Collection.findOne({ networkId: element.networkId }, excluded)
            .populate({
                path: 'customerUser'
            })
            .populate({
                path: 'companyUser'
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    findByEmail: (element, excludedKeysOnResult, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let excluded = {};
        excludedKeysOnResult.forEach(function (value) {
            excluded[value] = 0;
        });
        Collection.findOne({ email: element.email }, excluded)
            .populate({
                path: 'customerUser',
                populate: [
                    {
                        path: 'curriculum',
                        select: '-__v'
                    },
                    {
                        path: 'health',
                        select: '-__v'
                    }
                ]
            })
            .populate({
                path: 'companyUser',
                populate: {
                    path: 'company',
                    select: '-status -__v -families -manufacturers -categories -models',
                    populate: [
                        {
                            path: 'manufacturerFamilies',
                            select: '-status -__v -company',
                            populate: [
                                {
                                    path: 'families',
                                    select: '-status -__v'
                                },
                                {
                                    path: 'manufacturer',
                                    select: '-status -__v'
                                }
                            ]
                        },
                        {
                            path: 'address',
                            select: '-status -__v'
                        }
                    ]
                }
            })
            .populate({
                path: 'address'
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    findByFacebookId: (element, excludedKeysOnResult, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let excluded = {};
        excludedKeysOnResult.forEach(function (value) {
            excluded[value] = 0;
        });
        Collection.findOne({ facebookid: element.facebookid }, excluded)
            .populate({
                path: 'customerUser'
            })
            .populate({
                path: 'companyUser'
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    listAll: (fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let exclude = {
            password: 0
        }
        Collection.find({}, exclude)
            .populate({
                path: 'customerUser'
            })
            .populate({
                path: 'companyUser'
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    customersForApproval: (fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let exclude = {
            password: 0,
            isAcceptGeneralTerms: 0,
            isAcceptPurchaseTerms: 0,
            _id: 0,
            status: 0,
            secundaryPhone: 0,
            type: 0,
            __v: 0,
            Address: 0
        }
        Collection.find({ status: "WAITING_FOR_APPROVAL", type: "USER-CUSTOMER", primaryPhone: { $ne: null } }, exclude)
        .populate({
            path: 'customerUser'
        })  
        .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    listAllCustomer: (fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let exclude = {
            password: 0,
            isAcceptGeneralTerms: 0,
            isAcceptPurchaseTerms: 0,
            _id: 0,
            status: 0,
            lastName: 0,
            email: 0,
            secundaryPhone: 0,
            type: 0,
            __v: 0,
            customerUser: 0,
            companyUser: 0,
            Address: 0
        }
        Collection.find({ status: "ACTIVE", type: "USER-CUSTOMER", primaryPhone: { $ne: null } }, exclude)
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    listAllByType: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });
        let exclude = {
            password: 0
        }

        let filter = {};
        if (query.type) {
            filter.type = query.type;
        }

        Collection.find(filter, exclude)
            .populate({
                path: 'customerUser'
            })
            .populate({
                path: 'companyUser',
                populate: {
                    path: 'company'
                }
            })
            .then(params => {
                fncSuccess(params);
            }).catch((err) => {
                console.log(`caught the error1: ${err.stack}`);
                fncError(err.stack);
            });
    },
    clearAddrees: (element, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        Collection.updateOne({ address: element.address }, { $set: { address: null } })
            .then(response => {
                fncSuccess(response);
            }).catch((err) => {
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                console.log(`caught the error: ${err.stack}`);
                fncError(err.stack);
            });
    },
    changeEmail: async (element) => {
        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        try {
            let response = await Collection.updateOne({ email: element.params.email }, { $set: { email: element.body.email } })

            return response;
        } catch (err) {
            console.log(`caught the error: ${err.stack}`);
            throw err;
        }
    },
    searchCompanyUsers: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        let params = [];
        let paramsItems = [];
        let pipelineCompany = [];
        let pipelinecompanyUsers = [];
        paramsItems.push({ $eq: ["$_id", "$$idCompanyUser"] });

        let paramsCompany = [];
        paramsCompany.push({ $eq: ["$_id", "$$idCompany"] });

        let paramsCompanyType = [];
        paramsCompanyType.push({ status: 'ACTIVE' });
        if (query.typeService) {
            paramsCompanyType.push({ $in: [query.typeService, '$typeService'] });
        }

        if (query.companyId) {
            params.push({ 'companyUser.company': mongoose.Types.ObjectId(query.companyId) });
        }

        params.push({ status: 'ACTIVE' });


        let paramsName = [];
        paramsName.push({
            $or: [{
                $or: [{
                    fantasyName: {
                        $regex: query.name.toLowerCase(), $options: "i"
                    }
                }]
            },
            {
                $or: [{
                    socialName: {
                        $regex: query.name.toLowerCase(), $options: "i"
                    }
                }]
            }
            ]
        });

        pipelinecompanyUsers = [
            {
                $match: {
                    $expr: {
                        $and: paramsItems
                    }
                }
            },

        ]

        pipelineCompany = [
            {
                $lookup: {
                    from: 'addresses',
                    let: {
                        idAddress: '$$CURRENT.address'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ["$_id", "$$idAddress"] },
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'address'
                }
            },
            {
                $unwind: "$address"
            },
            {
                $match: {
                    $expr: {
                        $and: paramsCompany
                    }
                }
            },
            {
                $addFields: {
                    typeService: {
                        $cond: {
                            if: {
                                $ne: [
                                    { $type: "$typeService" }, "array"
                                ]
                            },
                            then: [],
                            else: "$typeService"
                        }
                    }
                }
            },
            {
                $match: {
                    $expr: {
                        $and: paramsCompanyType
                    }
                }
            },
            {
                $match: {
                    $and: paramsName
                }
            }
        ];

        if (!query.isHideDetailsCompany) {
            pipelinecompanyUsers.push({
                $lookup: {
                    from: 'companies',
                    let: {
                        idCompany: '$$CURRENT.company'
                    },
                    pipeline: pipelineCompany,
                    as: 'company'
                }
            });
            pipelinecompanyUsers.push({
                $unwind: "$company"
            });

            if (query.companyId) {
                params.push({ 'companyUser.company._id': mongoose.Types.ObjectId(query.companyId) });
            }
            params.push({ 'companyUser.company': { $ne: [] } });
            params.push({ companyUser: { $ne: null } });
            params.push({ companyUser: { $ne: [] } });
            params.push({ 'companyUser.company.status': 'ACTIVE' });
        }

        console.log("isHideDetailsManufecturerFamily: " + query.isHideDetailsManufecturerFamily);
        if (!query.isHideDetailsManufecturerFamily) {
            pipelineCompany.push(
                {
                    $lookup: {
                        from: 'manufacturer-families',
                        let: {
                            idsManufacturers: '$$CURRENT.manufacturerFamilies'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: [
                                            { $in: ['$_id', "$$idsManufacturers"] }
                                        ]
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'manufacturers',
                                    let: {
                                        idManufacturer: '$$CURRENT.manufacturer'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idManufacturer"] },
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'manufacturer'
                                }
                            },
                            {
                                $unwind: "$manufacturer"
                            },
                            {
                                $lookup: {
                                    from: 'part-families',
                                    let: {
                                        idFamilies: '$$CURRENT.families'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $in: ['$_id', "$$idFamilies"] }
                                                    ]
                                                }
                                            }
                                        }
                                    ],
                                    as: 'families'
                                }
                            }
                        ],
                        as: 'manufacturerFamilies'
                    }
                }
            );
        }

        let aggregator = [
            {
                $lookup: {
                    from: 'company-users',
                    let: {
                        idCompanyUser: '$$CURRENT.companyUser'
                    },
                    pipeline: pipelinecompanyUsers,
                    as: 'companyUser'
                }
            },
            {
                $unwind: "$companyUser"
            },
            {
                $match: {
                    $and: params
                }
            }
        ];

        if (query.limit != null && query.skip != null) {
            console.log('query.limit && query.skip');

            aggregator.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                    totalCount: [{
                        $count: 'count'
                    }]
                }
            });
        }

        Collection.aggregate(aggregator)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    },
    searchCustomerUsers: (query, fncSuccess) => {

        if (!isDbConnected) return fncError({ error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!' });

        let params = [];
        let paramsUser = [];
        let search = [];
        paramsUser.push({ $eq: ["$_id", "$$idCustomerUser"] });

        if (query.status) {
            params.push({ status: query.status });
        }else{
            params.push({ status: 'ACTIVE' });
        }

        if (query.name) {
            query.name = query.name.replace(' ', '').replace(' ', '').replace(' ', '').replace('.', '');
        }else{
            query.name = '';
        }
        params.push({ customerUser: { $ne: null } });
        params.push({ customerUser: { $ne: [] } });

        let paramsName = [];
        function diacriticSensitiveRegex(string = '') {
            return string.replace(/a/g, '[a,á,à,ä,ã,â]')
                .replace(/e/g, '[e,é,ë,ê]')
                .replace(/i/g, '[i,í,ï,î]')
                .replace(/c/g, '[c,ç]')
                .replace(/o/g, '[o,ó,ö,ò,õ,ô]')
                .replace(/u/g, '[u,ü,ú,ù,û]');
        }

        paramsName.push({
            $or: [{
                $or: [{
                    firstName: {
                        $regex: diacriticSensitiveRegex(query.name.toLowerCase()), $options: "i"
                    }
                }]
            },
            {
                $or: [{
                    lastName: {
                        $regex: diacriticSensitiveRegex(query.name.toLowerCase()), $options: "i"
                    }
                }]
            }
            ]
        });

        if (query.company) {
            params.push({ 'customerUser.company': mongoose.Types.ObjectId(query.company) });

            if (query.statusWithCompany) {
                params.push({ 'customerUser.statusWithCompany': query.statusWithCompany });
            }

            search = [
 
                {
                    $match: {
                        $and: paramsName
                    }
                },
                {
                    $lookup: {
                        from: 'customer-users',
                        let: {
                            idCustomerUser: '$$CURRENT.customerUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: { $eq: ["$_id", "$$idCustomerUser"] }
                                    }
                                }
                            },
                            // {
                            //     $lookup: {
                            //         from: 'companies',
                            //         let: {
                            //             idCompany: '$$CURRENT.company'
                            //         },
                            //         pipeline: [
                            //             {
                            //                 $match: {
                            //                     $expr: {
                            //                         $and: [
                            //                             { $eq: ["$_id", "$$idCompany"] }
                            //                         ]
                            //                     }
                            //                 }
                            //             },
                                            // { $project: { fantasyName: "$fantasyName", email: "$email", primaryPhone: "$primaryPhone"} },
                            //         ],
                            //         as: 'company'
                            //     }
                            // },
                            // {
                            //     $unwind: {
                            //         "path": "$company",
                            //         "preserveNullAndEmptyArrays": true
                            //     }
                            // },
                        ],
                        as: 'customerUser'
                    }
                },
                {
                    $unwind: {
                        "path": "$customerUser",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $match: {
                        $and: params
                    }
                },
            ]
        } else {
            search = [
                {
                    $match: {
                        $and: params
                    }
                },
                {
                    $match: {
                        $and: paramsName
                    }
                },
                {
                    $lookup: {
                        from: 'customer-users',
                        let: {
                            idCustomerUser: '$$CURRENT.customerUser'
                        },
                        pipeline: [
                            {
                                $match: {
                                    $expr: {
                                        $and: paramsUser
                                    }
                                }
                            },
                            {
                                $lookup: {
                                    from: 'companies',
                                    let: {
                                        idCompany: '$$CURRENT.company'
                                    },
                                    pipeline: [
                                        {
                                            $match: {
                                                $expr: {
                                                    $and: [
                                                        { $eq: ["$_id", "$$idCompany"] }
                                                    ]
                                                }
                                            }
                                        },
                                        { $project: { fantasyName: "$fantasyName", email: "$email", primaryPhone: "$primaryPhone"} },
                                    ],
                                    as: 'company'
                                },
                            },
                            {
                                $unwind: {
                                    "path": "$company",
                                    "preserveNullAndEmptyArrays": true
                                }
                            },

                        ],
                        as: 'customerUser'
                    }
                },
                {
                    $unwind: "$customerUser"
                }
                
            ]
        }

        if (query.limit != null && query.skip != null) {
            search.push({
                $facet: {
                    paginatedResults: [{ $skip: query.skip }, { $limit: query.limit }],
                    totalCount: [{
                        $count: 'count'
                    }
                    ]
                }
            });
        }

        Collection.aggregate(search)
            .then(obj => {
                fncSuccess(obj);
            }).catch(err => {
                console.log(`caught the error: ${err}`);
                if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId') {
                    return fncSuccess(null);
                }
                fncSuccess(err.stack);
            });
    }
};

module.exports = newModule;