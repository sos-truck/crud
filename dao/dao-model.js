const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

// Load Model
require('../models/Models');

const Collection = mongoose.model('models');

var newModule = {
    listByType: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.find({type: query.type, status: mapTypes.ACTIVE})
        .sort({name: 1})
        .then(params => {
            fncSuccess(params);
        }).catch((err) => {
            console.log(`caught the error1: ${err.stack}`);
            fncError(err.stack);
        });
    },
    listByManufacturer: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});
        Collection.find({manufacturer: query.manufacturer, status: mapTypes.ACTIVE})
        .sort({name: 1})
        .then(params => {
            fncSuccess(params);
        }).catch((err) => {
            console.log(`caught the error1: ${err.stack}`);
            fncError(err.stack);
        });
    },
};

module.exports = newModule;