const mongoose = require('mongoose');
const mapTypes = require('../helpers/map-status');

// Load Model
require('../models/SystemUsers');
require('../models/CustomerUsers');

require("../models/Curriculums");
const Collection = mongoose.model('curriculums');

var newModule = {
    filter: (query, fncSuccess, fncError) => {
        if (!isDbConnected) return fncError({error: '## CRUD ERROR TO CONNECT TO DATABASE!!!!!'});

        let params = [];

        if (query.status) {
            params.push('status',query.status);
        }else{
            params.push({status : 'ACTIVE'});
        }

        search = [
            {
                $lookup: {
                    from: 'system-users',
                    let: {
                        idSystemUser: '$$CURRENT.trucker'
                    },
                    pipeline: [
                        {
                            $match: {
                                $expr:{
                                    $and: [
                                        { $eq: [ "$_id", "$$idSystemUser" ] },
                                    ]
                                }
                            }
                        }
                    ],
                    as: 'trucker'
                }
            },
            {
                $unwind:"$trucker"
            },
            {
                $match: {
                    $expr:{
                        $and: params
                    }
                }
            }
        ]
    
    
        Collection.aggregate (search)
        .then(obj => {
            fncSuccess(obj);
        }).catch(err => {
            console.log(`caught the error: ${err}`);
            if (`${err.name}` == 'CastError' && `${err.kind}` == 'ObjectId'){
                return fncSuccess(null);
            }
            fncSuccess(err.stack);
        });
    }
};

module.exports = newModule;