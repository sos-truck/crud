const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CategoriesSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    family: {
        type: Schema.Types.ObjectId,
        ref:'part-families',
        required: false
    },
    type: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('categories', CategoriesSchema);