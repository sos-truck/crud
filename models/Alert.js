const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const AlertsSchema = new Schema({
    systemUser: {
        type: Schema.Types.ObjectId,
        ref:'system-users',
        required: true
    },
    latitude: {
        type: Number,
        require: true
    },
    longitude: {
        type: Number,
        require: true
    },
    date: {
        type: Date,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('alerts', AlertsSchema);