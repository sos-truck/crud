const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CustomerUsersSchema = new Schema({
    dateBirth: {
        type: Date,
        require: false
    },
    gender: {
        type: String,
        require: false
    },
    subtype: {
        type: String,
        require: true
    },
    document: {
        type: String,
        require: false
    },
    plate: {
        type: String,
        require: false
    },
    cnh: {
        type: String,
        require: false
    }, 
    rg: {
        type: String,
        require: false
    },
    obsCNH: {
        type: String,
        require: false
    },  
    cnhPhotoURL: {
        type: String,
        require: false
    },
    selfiePhotoURL: {
        type: String,
        require: false
    },
    company: {
        type: Schema.Types.ObjectId,
        ref:'companies',
        required: false
    },
    status: {
        type: String,
        require: true
    },
    statusWithCompany: {
        type: String,
        require: false
    },
    curriculum: {
        type: Schema.Types.ObjectId,
        ref:'curriculums',
        required: false
    },
    health: {
        type: Schema.Types.ObjectId,
        ref:'healths',
        required: false
    },
    geolocation: {
        type: Schema.Types.ObjectId,
        ref:'geolocations',
        required: false
    },
});

mongoose.model('customer-users', CustomerUsersSchema);