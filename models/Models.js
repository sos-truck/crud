const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const ModelsSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        require: true
    },
    fipeCode: {
        type: String,
        require: false
    },
    manufacturer: {
        type: Schema.Types.ObjectId,
        ref:'manufacturers',
        required: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('models', ModelsSchema);