const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const PartFamiliesSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    type: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('part-families', PartFamiliesSchema);