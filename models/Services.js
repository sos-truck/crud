const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const ServicesSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    price: {
        type: Number,
        require: true
    },
    companyUser: {
        type: Schema.Types.ObjectId,
        ref:'company-users',
        required: true
    },
    serviceCategory: {
        type: Schema.Types.ObjectId,
        ref:'service-categories',
        required: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('services', ServicesSchema);