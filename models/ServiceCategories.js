const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const ServiceCategoriesSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('service-categories', ServiceCategoriesSchema);