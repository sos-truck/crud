const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const GasstationSchema = new Schema({

    latlng: {
        latitude: {
            type: Number,
            require: true
        },
        longitude: {
            type: Number,
            require: true
        },
    },
    name: {
        type: String,
        require: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('gasstations', GasstationSchema);