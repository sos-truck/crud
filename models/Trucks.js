const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const TrucksSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    manufacturer: {
        type: Schema.Types.ObjectId,
        ref:'manufacturers',
        required: false
    }, 
    pbt: {
        type: Number,
        required: false
    },
    groupType: {
        type: String,
        required: false
    }, 
    model: {
        type: Schema.Types.ObjectId,
        ref:'models',
        default: null,
        required: false
    },
    year: {
        type: Number,
        require: false
    },
    yearModel: {
        type: Number,
        require: false
    },
    actualKM: {
        type: Number,
        require: false
    },
    mouths: {
        type: Number,
        require: false
    },
    plate: {
        type: String,
        require: true
    },
    renavam: {
        type: String,
        require: false
    },
    color: {
        type: String,
        require: false
    },
    traction: {
        type: String,
        require: false
    },
    chassi: {
        type: String,
        require: false
    },
    systemUser: {
        type: Schema.Types.ObjectId,
        ref:'system-users',
        required: false
    },
    truckDriver: {
        type: Schema.Types.ObjectId,
        ref:'system-users',
        required: false
    },
    company: {
        type: Schema.Types.ObjectId,
        ref:'companies',
        required: false
    },
    truckTrailers: [{
        type: Schema.Types.ObjectId,
        ref:'trucks',
        required: false
    }],
    certifies: [{
        type: Schema.Types.ObjectId,
        ref:'certified-items',
        required: false
    }],
    vehicleType: {
        type: String,
        require: true
    },
    document: {
        type: String,
        require: false
    },
    documentPhotoURL: {
        type: String,
        require: false
    },
    isOwner: {
        type: Boolean,
        require: false
    },
    status: {
        type: String,
        require: true
    },
    specie:{
        type: String,
        require: false
    },
    responsableCompanyPerson:{
        type: String,
        require: false
    },
    contactCompanyPhone:{
        type: String,
        require: false
    },
    fleetDriverName:{
        type: String,
        require: false
    }
   
});

mongoose.model('trucks', TrucksSchema);