const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const ChatMessages = new Schema({
    date: {
        type: Date,
        default: new Date
    },
    company: {
        type: Schema.Types.ObjectId,
        ref:'companies',
        required: false
    },
    trucker: {
        type: Schema.Types.ObjectId,
        ref:'system-users',
        required: false
    },
    order: {
        type: Schema.Types.ObjectId,
        ref:'orders',
        required: false
    },
    msg: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('chat-messages', ChatMessages);