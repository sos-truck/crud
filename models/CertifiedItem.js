const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CertifiedItemSchema = new Schema({
    certified: {
        type: Schema.Types.ObjectId,
        ref:'certifies',
        required: true
    },
    expiredDate: {
        type: Date,
        required: false
    },
    shippingDate: {
        type: Date,
        required: false
    },
    inspectDate: {
        type: Date,
        required: false
    },
    groupNumber: {
        type: String,
        required: false
    },
    barcodeNumber: {
        type: String,
        required: false
    },
    cippNumber: {
        type: String,
        required: false
    },
    equipmentNumber: {
        type: String,
        required: false
    },
    photoURL: {
        type: String,
        require: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('certified-items', CertifiedItemSchema);