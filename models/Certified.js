const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CertifiedSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: false
    },
    type: [{
        type: String,
        require: false
    }],  
    status: {
        type: String,
        require: true
    }
});

mongoose.model('certifies', CertifiedSchema);