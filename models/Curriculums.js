const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CurriculumUserSchema = new Schema({

    trucker: {
        type: Schema.Types.ObjectId,
        ref:'system-users',
        required: true
    },
    expirience: {
      type: String,
      require: false
    },
    expirienceAditional: {
      type: String,
      require: false
    },
    cnhCategory: {
      type: String,
      require: false
    },
    additionalCourse: {
      type: String,
      require: false
    },
    date: {
      type: Date,
      require: false
    },
    status: {
      type: String,
      require: false
  },
    type: {
    type: String,
    require: false
  },
  cep: {
    type: String,
    require: false
},
  
});

mongoose.model('curriculums', CurriculumUserSchema);