const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const ManufacturersSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('manufacturers', ManufacturersSchema);