const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const PartsSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    },
    photoURL: {
        type: String,
        require: false
    },
    amount: {
        type: Number,
        require: false
    },
    price: {
        type: Number,
        require: true
    },
    companyUser: {
        type: Schema.Types.ObjectId,
        ref:'company-users',
        required: true
    },
    partGroup: {
        type: Schema.Types.ObjectId,
        ref:'part-groups',
        required: true
    },
    partFamily: {
        type: Schema.Types.ObjectId,
        ref:'part-families',
        required: true
    },
    partBrand: {
        type: Schema.Types.ObjectId,
        ref:'part-brands',
        required: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('parts', PartsSchema);