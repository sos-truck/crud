const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CompanyUserSchema = new Schema({
    subtype: {
        type: String,
        require: true
    },
    company: {
        type: Schema.Types.ObjectId,
        ref:'companies',
        required: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('company-users', CompanyUserSchema);