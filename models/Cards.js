const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CardsSchema = new Schema({
    hash: {
        type: String,
        required: true
    },
    document: {
        type: String,
        require: false
    },
    name: {
        type: String,
        require: false
    },
    fantasyName: {
        type: String,
        require: false
    },
    documentType: {
        type: String,
        require: false
    },
    lastDigits: {
        type: String,
        required: true
    },
    isMain: {
        type: Boolean,
        required: true
    },
    company: {
        type: Schema.Types.ObjectId,
        ref:'companies',
        required: false
    },
    systemUser: {
        type: Schema.Types.ObjectId,
        ref:'system-users',
        required: true
    },
    paymentType: {
        type: String,
        required: false
    },
    status: {
        type: String,
        required: true
    }
});

mongoose.model('cards', CardsSchema);