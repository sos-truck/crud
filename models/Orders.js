const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const OrdersSchema = new Schema({
    manufacturer: {//descontinuado
        type: Schema.Types.ObjectId,
        ref: 'manufacturers',
        required: false
    },
    family: {//descontinuado
        type: Schema.Types.ObjectId,
        ref: 'part-families',
        required: false
    },
    certified: {//descontinuado
        type: Schema.Types.ObjectId,
        ref: 'certifies',
        required: false
    },
    serviceFamilies: {//descontinuado
        type: Schema.Types.ObjectId,
        ref: 'part-families',
        required: false
    },
    latitude: {//descontinuado
        type: Number,
        require: false
    },
    longitude: {//descontinuado
        type: Number,
        require: false
    },
    distance: {//descontinuado
        type: Number,
        require: false
    },
    description: {//descontinuado
        type: String,
        require: true
    },
    typeOrder: { //descontinuado
        type: String,
        require: true
    },
    company: {
        type: Schema.Types.ObjectId,
        ref: 'companies',
        required: true
    },
    systemUser: {
        type: Schema.Types.ObjectId,
        ref: 'system-users',
        required: false
    },  
    trucker: {
        type: Schema.Types.ObjectId,
        ref: 'system-users',
        required: false
    },
    isSearchByCompany: {
        type: Boolean,
        require: false
    },
    truck: {
        type: Schema.Types.ObjectId,
        ref: 'trucks',
        required: false
    },
    orderHolder: {
        type: Schema.Types.ObjectId,
        ref: 'orders-holder',
        required: false
    },
    statusOrder: {
        type: String,
        require: true
    },
    dateOrder: {
        type: Date,
        require: true
    },
    total: {
        type: Number,
        require: false
    },
    discount: {
        type: Number,
        require: false
    },
    paymentMethod: {
        type: String,
        require: false
    },
    paymentType: {
        type: String,
        require: false
    },
    transactionStatusCode: {
        type: String,
        require: false
    },
    transactionStatusDescription: {
        type: String,
        require: false
    },
    deliveryMethod: {
        type: String,
        require: false
    },
    justifyClose: {
        type: String,
        require: false
    },
    justifyReject: {
        type: String,
        require: false
    },
    userClose: {
        type: Schema.Types.ObjectId,
        ref: 'system-users',
        required: false
    },
    companyRequest: {
        type: Schema.Types.ObjectId,
        ref: 'companies',
        required: false
    },
    paymentObject: {
        type: Object,
        required: false
    },
    refundObject: {
        type: Object,
        required: false
    },
    companyIsInfosSufficient: {
        type: Boolean,
        require: false
    },
    companyAddionalInformations: {
        type: String,
        require: false
    },
    companyIsPartAvailable: {
        type: Boolean,
        require: false
    },
    companyIsPartOriginal: {
        type: Boolean,
        require: false
    },
    companyPartBrand: {
        type: String,
        require: false
    },
    companyIsDelivery: {
        type: Boolean,
        require: false
    },
    isTruckDecontaminated: {
        type: Boolean,
        require: false
    },
    quantity: {
        type: Number,
        require: false,
    },
    truckModel: {
        type: Schema.Types.ObjectId,
        ref: 'models',
        required: false
    },
    truckYear: {
        type: String,
        require: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('orders', OrdersSchema);