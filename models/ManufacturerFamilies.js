const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const ManufacturerFamiliesSchema = new Schema({
    manufacturer: {
        type: Schema.Types.ObjectId,
        ref:'manufacturers',
        required: true
    },
    families: [{
        type: Schema.Types.ObjectId,
        ref:'part-families',
        required: true
    }],
    company: {
        type: Schema.Types.ObjectId,
        ref:'companies',
        required: true
    },
    type: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('manufacturer-families', ManufacturerFamiliesSchema);