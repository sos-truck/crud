const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const WorkSchema = new Schema({
    monStart: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    monEnd: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    tueStart: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    tueEnd: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    wedStart: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    wedEnd: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    thuStart: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    thuEnd: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    friStart: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    friEnd: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    satStart: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    satEnd: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    sunStart: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    sunEnd: {
        HH: {
            type: String
        },
        mm: {
            type: String
        }
    },
    isExternalMeet: {
        type: Boolean,
        required: true
    },
    paymentMethods: [{
        type: String,
        required: true
    }],
    company:{
        type: Schema.Types.ObjectId,
        ref:'companies',
        required: true
    },
    typeAccountBank: {
        type: String,
        required: false
    },
    bankNumber: {
        type: Number,
        required: false
    },
    bankAgency: {
        type: String,
        required: false
    },
    bankAgencyVerificationCode: {
        type: String,
        required: false
    },
    bankAccountType: {
        type: String,
        required: false
    },
    bankAccountNumber: {
        type: String,
        required: false
    },
    bankAccountVerificationCode: {
        type: String,
        required: false
    },
    bankDocument: {
        type: String,
        required: false
    },
    bankDocumentType: {
        type: String,
        required: false
    },
    bankNameOwner: {
        type: String,
        required: false
    },
    maxInstalments: {
        type: Number,
        required: false
    },
    status: {
        type: String,
        required: true
    }
});

mongoose.model('company-add-infos', WorkSchema);