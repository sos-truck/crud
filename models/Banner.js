const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const BannersSchema = new Schema({
    company: {
        type: String,
        require: true
    },
    bannerURL: {
        type: String,
        require: true
    },
    siteURL: {
        type: String,
        require: false
    },
    time: {
        type: String,
        require: false
    },
    priority: {
        type: String,
        require: false
    },
    date: {
        type: Date,
        require: true
    },
    status: {
        type: String,
        require: true
    },

});

mongoose.model('banners', BannersSchema);