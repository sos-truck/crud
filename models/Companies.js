const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const CompanySchema = new Schema({
    fantasyName: {
        type: String,
        required: true
    },
    socialName: {
        type: String,
        require: true
    },
    document: {
        type: String,
        required: true
    },
    stateSubscription: {
        type: String,
        required: false
    },
    logo: {
        type: String,
        required: false
    },
    email: {
        type: String,
        require: false
    },
    primaryPhone: {
        type: String,
        require: false
    },
    secondaryPhone: {
        type: String,
        require: false
    },
    talkTo:{
        type: String,
        require: false
    },
    isBranch: {
        type: Boolean,
        require: false
    },
    mainCompany:{
        type: String,
        require: false
        // type: Schema.Types.ObjectId,
        // ref:'companies',
        // required: false
    },
    address:{
        type: Schema.Types.ObjectId,
        ref:'addresses',
        required: false
    },
    serviceFamilies: [{
        type: Schema.Types.ObjectId,
        ref:'part-families',
        required: false
    }],
    certifies: [{
        type: Schema.Types.ObjectId,
        ref:'certifies',
        required: false
    }],
    manufacturerFamilies: [{
        type: Schema.Types.ObjectId,
        ref:'manufacturer-families',
        required: false
    }],
    typeService: [{
        type: String,
        require: true
    }],
    recipientCode: {    
        type: String,
        required: false,
    },
    paymentType: {    
        type: String,
        required: false,
    },
    percentageCertified: {    
        type: Number,
        required: false,
        default: 85
    },
    date: {    
        type: Date,
        required: false,
    },
    status: {
        type: String,
        required: true
    }
});

mongoose.model('companies', CompanySchema);