const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const QuotesSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('quotes', QuotesSchema);