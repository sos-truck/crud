const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const PartBrandsSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('part-brands', PartBrandsSchema);