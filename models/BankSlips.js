const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const bankSplitsSchema = new Schema({
    company: {
        type: Schema.Types.ObjectId,
        ref: 'companies',
        required: true
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: 'orders',
        required: false
    },
    createDate: {
        type: Date,
        default: new Date,
        required: false
    },
    dueDate: {
        type: Date,
        default: new Date,
        required: false
    },
    month: {
        type: String,
        require: false
    },
    year: {
        type: String,
        require: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('bank-slips', bankSplitsSchema);