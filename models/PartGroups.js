const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const PartGroupsSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('part-groups', PartGroupsSchema);