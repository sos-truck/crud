const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const SystemUserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false
    },
    password: {
        type: String,
        require: false
    },
    primaryPhone: {
        type: String,
        require: false
    },
    secundaryPhone: {
        type: String,
        require: false
    },
    type: {
        type: String,
        require: true
    },
    customerUser: {
        type: Schema.Types.ObjectId,
        ref: 'customer-users',
        required: false
    },
    companyUser: {
        type: Schema.Types.ObjectId,
        ref: 'company-users',
        required: false
    },
    address: {
        type: Schema.Types.ObjectId,
        ref: 'addresses',
        required: false
    },
    facebookid: {
        type: String,
        require: false
    },
    isAcceptGeneralTerms: {
        type: Boolean,
        require: false,
        default: false
    },
    isAcceptPurchaseTerms: {
        type: Boolean,
        require: false,
        default: false
    },
    networkId: {
        type: String,
        require: false
    },
    networkType: {
        type: String,
        require: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('system-users', SystemUserSchema);