const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const HealthsSchema = new Schema({
trucker: {
    type: Schema.Types.ObjectId,
    ref:'system-users',
    required: true
},
systolicPressure: {
  type: String,
  require: false
},
diastolicPressure: {
  type: String,
  require: false
},
abdominalWaist: {
  type: String,
  require: false
},
temperature: {
  type: Number,
  require: false
},
saturation: {
  type: Number,
  require: false
},
diabetes: {
  type: Boolean,
  require: false
},
bloodGlucose: {
  type: Number,
  require: false
},
age:{
  type: Number,
  require: false
},
heartRate:{
  type: Number,
  require: false
},
obs: {
  type: String,
  require: false
},
date: {
  type: Date,
  require: false
},
status: {
  type: String,
  require: false
}
});

mongoose.model('healths', HealthsSchema);