const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const OrdersHolderSchema = new Schema({
    manufacturer: {
        type: Schema.Types.ObjectId,
        ref: 'manufacturers',
        required: false
    },
    family: {
        type: Schema.Types.ObjectId,
        ref: 'part-families',
        required: false
    },
    certified: {
        type: Schema.Types.ObjectId,
        ref: 'certifies',
        required: false
    },
    serviceFamilies: {
        type: Schema.Types.ObjectId,
        ref: 'part-families',
        required: false
    },
    typeOrder: {
        type: String,
        require: true
    },
    truck: {
        type: Schema.Types.ObjectId,
        ref: 'trucks',
        required: false
    },
    latitude: {
        type: Number,
        require: false
    },
    longitude: {
        type: Number,
        require: false
    },
    description: {
        type: String,
        require: true
    },
    orders: [{
        type: Schema.Types.ObjectId,
        ref: 'orders',
        required: true
    }],
    systemUser: {
        type: Schema.Types.ObjectId,
        ref: 'system-users',
        required: true
    },
    trucker: {
        type: Schema.Types.ObjectId,
        ref: 'system-users',
        required: true
    },
    company: {
        type: Schema.Types.ObjectId,
        ref: 'companies',
        required: false
    },
    isSearchByCompany: {
        type: Boolean,
        require: false
    },
    justifyClose: {
        type: String,
        require: false
    },
    isTruckDecontaminated: {
        type: Boolean,
        require: false
    },
    dateOrder: {
        type: Date,
        require: true
    },
    statusOrder: {
        type: String,
        require: true
    },
    truckModel: {
        type: Schema.Types.ObjectId,
        ref: 'models',
        required: false
    },
    truckYear: {
        type: String,
        require: false
    },
    status: {
        type: String,
        require: true
    }
});

mongoose.model('orders-holder', OrdersHolderSchema);