const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const AddressSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    number: {
        type: String,
        require: false
    },
    neighborhood: {
        type: String,
        required: true
    },
    cep: {
        type: String,
        require: true
    },
    complement: {
        type: String,
        required: false
    },
    ibge: {
        type: String,
        required: false
    },
    gia: {
        type: String,
        required: false
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    location: {
        type: { type: String },
        coordinates: []
    },
    status: {
        type: String,
        required: true
    }
});

// mongoose

AddressSchema.index({ location: "2dsphere" });
Collection = mongoose.model('addresses', AddressSchema);

AddressSchema.on('index', function(err) {
    if (err) {
        console.error('###### User index error: %s', err);
    } else {
        console.info('###### User indexing complete');
    }
});