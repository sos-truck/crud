# Micro Services

## Project setup
```
npm install
```

### Run the project - It`s recommended to install nodemon - https://nodemon.io/
```
nodemon
OR
npm start
```

### Environment Variables
```
________ LINUX OR MAC

export SOST_DB_URI="mongodb://sosdev:sosdev123@ds021771.mlab.com:21771/sos-db-dev"
export SOST_SECRET="221SSDDFss&sKOdg7MsddeeqqDGHjjju8892###ZkdzuSE6PURSJGRp2aVu01NlSYeKp3psR172"
export SOST_EMAIL="sosprojectapp@gmail.com"
export SOST_EMAIL_PASSWORD="Sucesso@2019"
export SOST_FACEBOOK_SECRET="578ee80dff5ed044d738d73dbccb1fe5"
export SOST_FACEBOOK_CLIENT_ID="387705478753812"
export SOST_ELK_SECRET_TOKEN=""
export SOST_ELK_SERVER_URL=""
export SOST_DOMAIN_API_GATEWAY="http://localhost:8081"
export SOST_DOMAIN_MS_ADDRESS="http://localhost"
export SOST_DOMAIN_MS_ORDERS="http://localhost"
export SOST_DOMAIN_MS_PRODUCTS="http://localhost"
export SOST_DOMAIN_MS_USER="http://localhost"

________ WINDOWS

set SOST_DB_URI="mongodb://sosdev:sosdev123@ds021771.mlab.com:21771/sos-db-dev"
set SOST_SECRET="221SSDDFss&sKOdg7MsddeeqqDGHjjju8892###ZkdzuSE6PURSJGRp2aVu01NlSYeKp3psR172"
set SOST_EMAIL="sosprojectapp@gmail.com"
set SOST_EMAIL_PASSWORD="Sucesso@2019"
set SOST_FACEBOOK_SECRET="578ee80dff5ed044d738d73dbccb1fe5"
set SOST_FACEBOOK_CLIENT_ID="387705478753812"
set SOST_ELK_SECRET_TOKEN=""
set SOST_ELK_SERVER_URL=""
set SOST_DOMAIN_API_GATEWAY="http://localhost:8081"
set SOST_DOMAIN_MS_ADDRESS="http://localhost"
set SOST_DOMAIN_MS_ORDERS="http://localhost"
set SOST_DOMAIN_MS_PRODUCTS="http://localhost"
set SOST_DOMAIN_MS_USER="http://localhost"

```

### Script to run all micro services (It`s necessary to have the 'nodemon' installed in npm global Environment and the 'ttab' module too)
```
 ./run-projects
```

### Tips
```
- After update the version of any dependences, delete de node_modules and force update the package-lock.json.
- Only use the local helpers when you have been updating the files.
```
