const dao = require('../../dao/dao-truck');

var newModule = {
    methods: {
        listWithoutCertifie: (fncResult) => {
            dao.listWithoutCertifie((response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listAllWithCertifieExpired: (fncResult) => {
            dao.listAllWithCertifieExpired((response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        filter: async (element, fncResult) => {
            try {
                let response = await dao.filter(element);
    
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    if (msg.results){
                        msg.length = msg.results.length;
                    }
                    msg.results = response;
                    if (fncResult)
                        return fncResult(msg);

                    return msg;    
                }else {
                    if (fncResult)
                        return fncResult(instantiateMessage(404, 'response.not.found'), true);

                    return instantiateMessage(404, 'response.not.found');
                } 
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;    
            }
        },
        listAllByFilters: async (element, fncResult) => {
            try {
                let response = await dao.listAllByFilters(element);
    
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    if (msg.results){
                        msg.length = msg.results.length;
                    }
                    msg.results = response;
                    if (fncResult)
                        return fncResult(msg);

                    return msg;    
                }else {
                    if (fncResult)
                        return fncResult(instantiateMessage(404, 'response.not.found'), true);

                    return instantiateMessage(404, 'response.not.found');
                } 
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;    
            }
        },
        filterCertifieExpired: async (element, fncResult) => {
            dao.filterCertifieExpired(element, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listGroupByManufacturer: async (query, fncResult) => {
            try {
                let response = await dao.listGroupByManufacturer(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        }
    }
};

module.exports = newModule;