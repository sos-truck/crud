
const dao = require('../../dao/dao-indication-code');

var newModule = {
    methods: {
        findOwnerByCode: (element, fncResult) => {
            dao.findOwnerByCode(element, (response) => {
                const msg = instantiateMessage(200, 'hedz.msg.success');
                msg.result = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'hedz.msg.error.generic');
                msg.systemError = err.stack;
                fncResult(msg);
            });
        },
        filter: (query, fncResult) => {
            dao.filter(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        getMyCupom: (query, fncResult) => {
            dao.getMyCupom(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
    }
};

module.exports = newModule;