
const dao = require('../../dao/dao-company');
const mapStatus = require('../../helpers/map-status');

var newModule = {
    methods: {
        listAllActive: (fncResult) => {
            dao.listAllActive((response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listAllByType: (query, fncResult) => {
            dao.listAllByType(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        getLocations: (query, fncResult) => {
            dao.getLocations(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        filterCompanies: (query, fncResult) => {
            dao.filterCompanies(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        loadById: (query, fncResult) => {
            dao.loadById(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        search: (query, fncResult) => {
            dao.search(query, (collection) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = collection;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        searchv2: (query, fncResult) => {
            dao.searchV2(query, (collection) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = collection;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        searchParts: (query, fncResult) => {
            dao.searchParts(query, (collection) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = collection;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        }
    }
};

module.exports = newModule;