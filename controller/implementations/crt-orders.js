
const dao = require('../../dao/dao-orders');

var newModule = {
    methods: {
        listByFilters: (query, fncResult) => {
            dao.listByFilters(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        changeStatusElements: async (element, fncResult) => {
            try {
                let response = await dao.changeStatusElements(element);
    
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;

                if (fncResult)
                    return fncResult(msg);

                return msg;    
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;
            }
        },
        getByHashPayment: async (element, fncResult) => {
            try {
                let response = await dao.getByHashPayment(element);
    
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;

                if (fncResult)
                    return fncResult(msg);

                return msg;    
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;
            }
        },
        findById: async (element, fncResult) => {
            try {
                let response = await dao.findById(element);
    
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;

                if (fncResult)
                    return fncResult(msg);

                return msg;    
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;
            }
        }
    }
};

module.exports = newModule;