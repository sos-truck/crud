
const dao = require('../../dao/dao-manufacturer-families');

var newModule = {
    methods: {
        deleteByCompany: async (element, fncResult) => {
            try {
                let response = await dao.deleteByCompany(element);
    
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;

                    if (fncResult)
                        return fncResult(msg);

                    return msg;    
                }else {
                    if (fncResult)
                        return fncResult(instantiateMessage(404, 'response.not.found'), true);

                    return instantiateMessage(404, 'response.not.found');
                } 
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                return msg;    
            }
        }
    }
};

module.exports = newModule;