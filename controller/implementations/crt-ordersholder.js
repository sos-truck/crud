
const dao = require('../../dao/dao-ordersHolder');

var newModule = {
    methods: {
        getHolderByOrder: async (query, fncResult) => {
            try {
                let response = await dao.getHolderByOrder(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        listByUserWithoutLookup: async (query, fncResult) => {
            
            try {
                let response = await dao.listByUserWithoutLookup(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        listByFilters: async (query, fncResult) => {
            
            try {
                let response = await dao.listByFilters(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        changeStatusElements: async (element, fncResult) => {
            try {
                let response = await dao.changeStatusElements(element);
    
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;

                if (fncResult)
                    return fncResult(msg);

                return msg;    
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;
            }
        },
        listByFiltersCompany: async (query, fncResult) => {
            
            try {
                let response = await dao.listByFiltersCompany(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        listGroupByManufacturer: async (query, fncResult) => {
            try {
                let response = await dao.listGroupByManufacturer(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },

        listGroupByService: async (query, fncResult) => {
            try {
                let response = await dao.listGroupByService(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        listGroupByFamily: async (query, fncResult) => {
            try {
                let response = await dao.listGroupByFamily(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        listGroupByCertifie: async (query, fncResult) => {
            try {
                let response = await dao.listGroupByCertifie(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        listGroupByDate: async (query, fncResult) => {
            try {
                let response = await dao.listGroupByDate(query);
                
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                if (fncResult){
                    return fncResult(msg);
                }
                return msg;

            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult){
                    return fncResult(msg);
                }
                throw msg;
            }
        },
        
    }
};

module.exports = newModule;