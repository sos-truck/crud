
const dao = require('../../dao/dao-certifies');

var newModule = {
    methods: {
        list: (query, fncResult) => {
            dao.list(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        }
    }
};

module.exports = newModule;