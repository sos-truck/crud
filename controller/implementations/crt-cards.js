
const dao = require('../../dao/dao-cards');

var newModule = {
    methods: {
        setAllMainToFalse: (element, fncResult) => {
            dao.setAllMainToFalse(element, (response) => {
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    fncResult(msg);
                }else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        setMain: (element, fncResult) => {
            dao.setMain(element, (response) => {
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    fncResult(msg);
                }else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        getMain: (element, fncResult) => {
            dao.getMain(element, (response) => {
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;
                    fncResult(msg);
                }else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listAllByUser: (element, fncResult) => {
            dao.listAllByUser(element, (response) => {
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.results = response;
                    fncResult(msg);
                }else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        deleteByUser: async (element, fncResult) => {
            try {
                let response = await dao.deleteByUser(element);
    
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;

                    if (fncResult)
                        return fncResult(msg);

                    return msg;    
                }else {
                    if (fncResult)
                        return fncResult(instantiateMessage(404, 'response.not.found'), true);

                    return instantiateMessage(404, 'response.not.found');
                } 
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;    
            }
        },
        companySetAllMainToFalse: (element, fncResult) => {
            dao.companySetAllMainToFalse(element, (response) => {
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    fncResult(msg);
                }else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        companyGetMain: (element, fncResult) => {
            dao.companyGetMain(element, (response) => {
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;
                    fncResult(msg);
                }else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listAllByCompany: (element, fncResult) => {
            dao.companyListAllByUser(element, (response) => {
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.results = response;
                    fncResult(msg);
                }else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        deleteByCompany: async (element, fncResult) => {
            try {
                let response = await dao.deleteByCompany(element);
    
                if (response){
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;

                    if (fncResult)
                        return fncResult(msg);

                    return msg;    
                }else {
                    if (fncResult)
                        return fncResult(instantiateMessage(404, 'response.not.found'), true);

                    return instantiateMessage(404, 'response.not.found');
                } 
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;    
            }
        }
    }
};

module.exports = newModule;