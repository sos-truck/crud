
const dao = require('../../dao/dao-systemUser');
const mapStatus = require('../../helpers/map-status');

var newModule = {
    methods: {

        listCustomerWhithoutTruck: (query, fncResult) => {
            dao.listCustomerWhithoutTruck(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        findByEmail: (element, exclude, fncResult) => {
            dao.findByEmail(element, exclude, (response) => {
                if (response) {
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;
                    fncResult(msg);
                } else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        findByNetworkId: (element, exclude, fncResult) => {
            dao.findByNetworkId(element, exclude, (response) => {
                if (response) {
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;
                    fncResult(msg);
                } else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        findByPhone: (element, exclude, fncResult) => {
            dao.findByPhone(element, exclude, (response) => {
                if (response) {
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;
                    fncResult(msg);
                } else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        findByFacebookId: (element, exclude, fncResult) => {
            dao.findByFacebookId(element, exclude, (response) => {
                if (response) {
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;
                    fncResult(msg);
                } else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listAll: (fncResult) => {
            dao.listAll((response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listAllCustomer: (fncResult) => {
            dao.listAllCustomer((response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        customersForApproval: (fncResult) => {
            dao.customersForApproval((response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        listAllByType: (query, fncResult) => {
            dao.listAllByType(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        clearAddrees: (element, fncResult) => {
            dao.clearAddrees(element, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        clearCard: (element, fncResult) => {
            dao.clearCard(element, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        changeEmail: async (element, fncResult) => {
            try {
                let response = await dao.changeEmail(element);

                const msg = instantiateMessage(200, 'response.msg.success');
                msg.result = response;

                if (fncResult)
                    return fncResult(msg);

                return msg;
            } catch (err) {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                if (fncResult)
                    return fncResult(msg);

                throw msg;
            }
        },
        checkPhone: (element, exclude, fncResult) => {
            dao.findByPhone(element, exclude, (response) => {
                if (response) {
                    const msg = instantiateMessage(200, 'response.msg.success');
                    msg.result = response;
                    fncResult(msg);
                } else {
                    return fncResult(instantiateMessage(404, 'response.not.found'), true);
                }
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        searchCompanyUsers: (query, fncResult) => {
            dao.searchCompanyUsers(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
        searchCustomerUsers: (query, fncResult) => {
            dao.searchCustomerUsers(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        }
    }
};

module.exports = newModule;