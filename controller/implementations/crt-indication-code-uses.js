
const dao = require('../../dao/dao-indication-code-uses');

var newModule = {
    methods: {
      
        filter: (query, fncResult) => {
            dao.filter(query, (response) => {
                const msg = instantiateMessage(200, 'response.msg.success');
                msg.results = response;
                fncResult(msg);
            }, (err) => {
                console.log(`caught the error: ${err.stack}`);
                const msg = instantiateMessage(500, 'response.msg.error.generic');
                msg.systemError = err.stack;
                return fncResult(msg);
            });
        },
    }
};

module.exports = newModule; 