const mongoose = require('mongoose');
const dao = require('../dao/dao-generic');

const crtGeneric = require('./implementations/crt-generic');
const crtSystemUser = require('./implementations/crt-systemUser');
const crtPart = require('./implementations/crt-part');
const crtPartFamilies = require('./implementations/crt-partFamilies');
const crtManufacturs = require('./implementations/crt-manufacturers');
const crtCategories = require('./implementations/crt-categories');
const crtModel = require('./implementations/crt-model');
const crtCertified = require('./implementations/crt-certifies');
const crtManufacturFamilies = require('./implementations/crt-manufacturer-families');
const crtCompany = require('./implementations/crt-company');
const crtWorkHours = require('./implementations/crt-companyAddInfos');
const crtOrders = require('./implementations/crt-orders');
const crtOrdersHolder = require('./implementations/crt-ordersholder');
const crtCertItems = require('./implementations/crt-certItems');
const crtTruck = require('./implementations/crt-truck');
const crtAlert = require('./implementations/crt-alert');
const crtGeolocation = require('./implementations/crt-geolocation');
const crtCard = require('./implementations/crt-cards');
const crtChat = require('./implementations/crt-chat');
const crtCurriculum = require('./implementations/crt-curriculums');
const crtIndicationCode = require('./implementations/crt-indication-code');
const crtIndicationCodeUses = require('./implementations/crt-indication-code-uses');
const crtHealths = require('./implementations/crt-healths');
const crtBankSplits = require('./implementations/crt-bank-splits');
const crtGasStation = require('./implementations/crt-gasstation');
const crtBanner = require('./implementations/crt-banners');

// DB Config
mongoose.set('useCreateIndex',true);
const db = require('../config/database');
mongoose.connect(db.mongoURI, {useNewUrlParser: true, useUnifiedTopology: true})
.then(() => {
  global.isDbConnected = true;
})
.catch(err => {
  global.isDbConnected = false;
  console.error(err); 
});

global.instantiateMessage = require('hp-sostruck-message').createObjMsg;

var newModule = {
    setCollection: (fileName, collectionName, primaryKey, excludedKeys) => {
        dao.setCollection(fileName, collectionName, primaryKey, excludedKeys);
    },
    getDAO: () => {
        return dao;
    },
    generic: crtGeneric.methods,
    systemuser: crtSystemUser.methods,
    part: crtPart.methods,
    partFamilies: crtPartFamilies.methods,
    manufacturers: crtManufacturs.methods,
    manufacturerFamilies: crtManufacturFamilies.methods,
    categories: crtCategories.methods,
    models: crtModel.methods,
    certifies: crtCertified.methods,
    company: crtCompany.methods,
    companyAddInfos: crtWorkHours.methods,
    orders: crtOrders.methods,
    ordersholder: crtOrdersHolder.methods,
    certItems: crtCertItems.methods,
    truck: crtTruck.methods,
    alert: crtAlert.methods,
    cards: crtCard.methods,
    chat: crtChat.methods,
    curriculums: crtCurriculum.methods,
    geolocation: crtGeolocation.methods,
    IndicationCode: crtIndicationCode.methods,
    IndicationCodeUses: crtIndicationCodeUses.methods,
    Healths: crtHealths.methods,
    bankSplit: crtBankSplits.methods,
    gasStation: crtGasStation.methods,
    crtBanner: crtBanner.methods
};

module.exports = newModule;